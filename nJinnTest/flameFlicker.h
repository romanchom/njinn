#pragma once

#include <Eigen/Dense>
#include <component.h>
#include <random>

class flameFlicker : public nJinn::component
{
private:
	Eigen::Vector3d mCenter;
	Eigen::Vector3d mVelocity;
	double mAmplitude;
public:
	flameFlicker(nJinn::gameObject * owner);
	void amplitude(double val) { mAmplitude = val; }
protected:
	virtual component * copyTo(nJinn::gameObject * target) const override;
	virtual void initialize() override;
	virtual void update() override;
};

