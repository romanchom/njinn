#pragma once

#include <component.h>
#include <Eigen/Dense>

class asteroidSpinner : public nJinn::component
{
	Eigen::Vector3d mAxis;
	double mSpeed;
public:
	asteroidSpinner(nJinn::gameObject * owner);
	virtual component * copyTo(nJinn::gameObject * target) const override { return nullptr; }
	virtual void initialize() override;
	virtual void update() override;
};

