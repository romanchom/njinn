#include "stdafx.h"

#include <application.h>

#include <config.h>
#include <gameObject.h>
#include <mathGeometry.h>
#include <input.h>
#include <texture.h>
#include <meshRenderer.h>
#include <mesh.h>
#include <rotatorComponent.h>
#include <camera.h>
#include <pointLightGS.h>
#include <pointLightMRST.h>
#include <pointLightMRMT.h>
#include <pointLightI.h>
#include <rand.h>
#include <gameTime.h>


#include "cameraAnimator.h"
#include "sponzaCameraOperator.h"
#include "flameFlicker.h"
#include "orbiter.h"
#include "lookAtComponent.h"
#include "asteroidSpinner.h"

using namespace nJinn;
using namespace Eigen;


class myGame : public nJinn::gameBase {
	void loadSponza() {
		gameObject * go = gameObject::create();
		go->position(0, 0, 0);
		meshRenderer * mr = go->addComponent<meshRenderer>();
		mr->mMesh = mesh::load(L"sponza.sbm");
		const double flickerAmp = 100;

		gameObject * light = gameObject::create();
		light->position(-180, -660, 150);
		light->addComponent<pointLightMRST>()->color(Vector3f(10000, 1000, 2000));
		light->addComponent<flameFlicker>()->amplitude(flickerAmp);

		light = gameObject::create();
		light->position(170, -660, 150);
		light->addComponent<pointLightMRMT>()->color(Vector3f(10000, 10000, 1000));
		light->addComponent<flameFlicker>()->amplitude(flickerAmp);

		light = gameObject::create();
		light->position(170, 440, 150);
		light->addComponent<pointLightI>()->color(Vector3f(1000, 10000, 1000));
		light->addComponent<flameFlicker>()->amplitude(flickerAmp);

		light = gameObject::create();
		light->position(-180, 440, 150);
		light->addComponent<pointLightGS>()->color(Vector3f(1000, 1000, 10000));
		light->addComponent<flameFlicker>()->amplitude(flickerAmp);
	}

	void makeAsteroidField()
	{
		const double size = 400;
		const int count = config::options["level"].as<int>();
		pTexture tex = texture::load(L"asteroid_diffuse.dds");
		pMesh m = mesh::load(L"asteroid2.sbm");
		const double increment = 2 * size / (count - 1);

		for (int i = 0; i < count; ++i) {
			gameObject * o = gameObject::create();
			o->position(nJinn::rand::uniform<double, 3>(-size, size));
			o->scale(nJinn::rand::uniform<double, 3>(4, 12));
			meshRenderer * mr = o->addComponent<meshRenderer>();
			mr->mMesh = m;
			mr->mTexture = tex;
			o->addComponent<asteroidSpinner>();
		}

		addRotatingLights();
	}

	void loadBigAssteroid() {
		pTexture tex = texture::load(L"asteroid_diffuse.dds");
		pMesh m = mesh::load(L"asteroid_crazyT.sbm");

		gameObject * o = gameObject::create();
		o->scale(80, 80, 80);
		meshRenderer * mr = o->addComponent<meshRenderer>();
		mr->mMesh = m;
		mr->mTexture = tex;

		addRotatingLights(600, 8000);
	}

	void addRotatingLights(double r = 500, double b = 4000) {
		orbiter * o;
		gameObject * light = gameObject::create();
		light->addComponent<pointLightMRST>()->color(b * Vector3f(10, 1, 2));
		o = light->addComponent<orbiter>();
		o->radius(r);
		o->speed(0.5);
		o->phase(0);

		light = gameObject::create();
		light->addComponent<pointLightMRMT>()->color(b * Vector3f(10, 10, 1));
		o = light->addComponent<orbiter>();
		o->radius(r);
		o->speed(0.5);
		o->phase(0.5 * M_PI);

		light = gameObject::create();
		light->addComponent<pointLightI>()->color(b * Vector3f(1, 10, 1));
		o = light->addComponent<orbiter>();
		o->radius(r);
		o->speed(0.5);
		o->phase(M_PI);

		light = gameObject::create();
		light->addComponent<pointLightGS>()->color(b * Vector3f(1, 1, 10));
		o = light->addComponent<orbiter>();
		o->radius(r);
		o->speed(0.5);
		o->phase(1.5 * M_PI);
	}

	double demoTime;
	virtual void OnInitialize() {
		gameObject * cam = gameObject::create();
		camera * c = cam->addComponent<camera>();
		c->nearPlane(0.01);
		c->farPlane(100000);
		
		int level = config::options["level"].as<int>();

		if (level == 0) {
			loadSponza();
			cam->position(0, -200, 100);
		}
		else if (level == -1) {
			loadBigAssteroid();
			cam->position(0, -20, 0);
		}
		else {
			makeAsteroidField();
			cam->position(0, -100, 0);
		}

		demoTime = config::options["demoTime"].as<int>() * 0.001;
		if (demoTime < 0) {
			cam->addComponent<sponzaCameraOperator>();
		}
		else {
			cam->position(0, 0, 340);
			orbiter * orb = cam->addComponent<orbiter>();
			orb->radius(650);
			orb->speed(0.1 * M_PI);
			cam->addComponent<lookAtComponent>();
		}
		demoTime = config::options["demoTime"].as<int>() * 0.001;
	}

	virtual void OnUpdate() override {
		if (demoTime > 0 && gameTime::get() > demoTime) {
			application::quit();
		}
	}
};

namespace po = boost::program_options;

_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR args, int nCmdShow)
{
	config::optionsDesc.add_options()
		("level", po::value<int>()->default_value(0), "level to load (0 - sponza, 1 - asteroid field)")
		("demoTime", po::value<int>()->default_value(-1), "time to run demo (ms, -1 - disabled)");
		
	return application::initialize<myGame>(hInstance, args);
}