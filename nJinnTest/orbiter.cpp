#include "stdafx.h"
#include "orbiter.h"

#include <gameObject.h>
#include <gameTime.h>

using namespace nJinn;
using namespace Eigen;

void orbiter::initialize()
{
	mCenter = mOwner->position();
}

void orbiter::update()
{
	double angle = mSpeed * gameTime::get() + mPhase;
	double s = sin(angle) * mRadius;
	double c = cos(angle) * mRadius;

	Vector3d p;
	p << s, c, 0;
	p += mCenter;
	mOwner->position(p);
}
