#include "stdafx.h"
#include "sponzaCameraOperator.h"

#include <input.h>
#include <gameTime.h>

#include <gameObject.h>

using namespace nJinn;
using namespace Eigen;

sponzaCameraOperator::sponzaCameraOperator(gameObject * owner) : component(owner, 0),
	mYaw(0),
	mPitch(0)
{}

void sponzaCameraOperator::update()
{
	double yawSpeed = 1 * gameTime::delta();
	double pitchSpeed = 1 * gameTime::delta();

	double dYaw = 0;
	double dPitch = 0;

	if (input::is(keyRight, pressed))	dYaw += yawSpeed;
	if (input::is(keyLeft, pressed))	dYaw -= yawSpeed;
	if (input::is(keyUp, pressed))		dPitch += pitchSpeed;
	if (input::is(keyDown, pressed))	dPitch -= pitchSpeed;

	mYaw += dYaw;
	mPitch += dPitch;

	Quaterniond rot(AngleAxisd(-mYaw, Vector3d::UnitZ()));
	rot *= Quaterniond(AngleAxisd(mPitch, Vector3d::UnitX()));
	mOwner->rotation(rot);

	double moveSpeed = 200 * gameTime::delta();
	double moveForward = 0;

	if (input::is(keyW, pressed)) moveForward += moveSpeed;
	if (input::is(keyS, pressed)) moveForward -= moveSpeed;

	if (moveForward != 0) {
		Vector3d forward;
		forward << sin(mYaw) * cos(mPitch), cos(mYaw) * cos(mPitch), sin(mPitch);

		Vector3d pos = mOwner->position();
		pos += forward * moveForward;
		mOwner->position(pos);
	}
}

component * sponzaCameraOperator::copyTo(gameObject * target) const
{
	return nullptr;
}

void sponzaCameraOperator::initialize()
{
}
