#include "stdafx.h"
#include "lookAtComponent.h"

#include <gameObject.h>
#include <mathGeometry.h>

lookAtComponent::lookAtComponent(nJinn::gameObject * owner) :
	component(owner, 1),
	mTarget(0, 0, 0)
{
}

void lookAtComponent::initialize()
{
}

void lookAtComponent::update()
{
	mOwner->rotation(nJinn::lookAtRotation<double>(mOwner->position(), mTarget, Eigen::Vector3d(0, 0, 1)));
}
