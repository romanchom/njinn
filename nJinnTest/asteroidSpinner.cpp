#include "stdafx.h"
#include "asteroidSpinner.h"

#include <rand.h>
#include <gameObject.h>
#include <gameTime.h>

asteroidSpinner::asteroidSpinner(nJinn::gameObject * owner) :
	component(owner, 0)
{
	mAxis = nJinn::rand::uniform<double, 3>(-1.0, 1.0);
	mAxis.normalize();
	mSpeed = nJinn::rand::uniform(-2.0, 2.0);
}

void asteroidSpinner::initialize()
{
}

void asteroidSpinner::update()
{
	Eigen::Quaterniond q;
	q = Eigen::AngleAxisd(mSpeed * nJinn::gameTime::get(), mAxis);
	mOwner->rotation(q);
}
