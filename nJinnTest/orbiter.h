#pragma once

#include <component.h>
#include <Eigen/Dense>

class orbiter : public nJinn::component
{
private:
	double mRadius;
	double mSpeed;
	double mPhase;
	Eigen::Vector3d mCenter;
public:
	orbiter(nJinn::gameObject * owner) : component(owner, 0) {}
	virtual component * copyTo(nJinn::gameObject * target) const override { return nullptr; };
	virtual void initialize() override;
	virtual void update() override;


	void phase(double val) { mPhase = val; }
	void radius(double val) { mRadius = val; }
	void speed(double val) { mSpeed = val; }
};

