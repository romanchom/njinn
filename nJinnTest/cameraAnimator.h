#pragma once

#include <Eigen/Dense>
#include <component.h>

class cameraAnimator : public nJinn::component
{
public:
	cameraAnimator(nJinn::gameObject * owner);
	double radius;
	Eigen::Vector3d center;
	double speed;
	virtual cameraAnimator * copyTo(nJinn::gameObject * target) const override;
protected:
	virtual void initialize() override;
	virtual void update() override;
};

