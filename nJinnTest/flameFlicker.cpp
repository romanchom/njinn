#include "stdafx.h"
#include "flameFlicker.h"

#include <gameObject.h>
#include <rand.h>
#include <gameTime.h>

using namespace nJinn;
using namespace Eigen;

flameFlicker::flameFlicker(nJinn::gameObject * owner) : component(owner, 0),
	mCenter(0, 0, 0),
	mVelocity(0, 0, 0)
{
}

component * flameFlicker::copyTo(gameObject * target) const
{
	return nullptr;
}

void flameFlicker::initialize()
{
	mCenter = mOwner->position();
}

void flameFlicker::update()
{
	Vector3d p = mOwner->position();
	Vector3d diff = mCenter - p;
	mVelocity += diff * (1 - exp(-gameTime::delta() * mAmplitude));
	mVelocity += mAmplitude * gameTime::delta() * nJinn::rand::uniform<double, 3>(-1, 1);
	mVelocity *= exp(-gameTime::delta());
	p += mVelocity * gameTime::delta();
	mOwner->position(p);
}
