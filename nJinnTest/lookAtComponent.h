#pragma once

#include <Eigen/Dense>
#include <component.h>

class lookAtComponent : public nJinn::component
{
	Eigen::Vector3d mTarget;
public:
	lookAtComponent(nJinn::gameObject * owner);


	virtual component * copyTo(nJinn::gameObject * target) const override { return nullptr; }
	virtual void initialize() override;
	virtual void update() override;
};

