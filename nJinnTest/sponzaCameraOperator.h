#pragma once

#include <component.h>

class sponzaCameraOperator : public nJinn::component
{
public:
	sponzaCameraOperator(nJinn::gameObject * owner);
protected:
	void update() override;
	virtual component * copyTo(nJinn::gameObject * target) const override;
	virtual void initialize() override;
private:
	double mYaw, mPitch;

};

