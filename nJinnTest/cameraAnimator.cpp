#include "stdafx.h"
#include "cameraAnimator.h"

#include <cmath>
#include <gameObject.h>
#include <gameTime.h>

cameraAnimator::cameraAnimator(nJinn::gameObject * owner) : component(owner, 0),
	radius(50),
	speed(1)
{}

cameraAnimator * cameraAnimator::copyTo(nJinn::gameObject * target) const
{
	return nullptr;
}

void cameraAnimator::initialize()
{
}

void cameraAnimator::update()
{
	double s, c;
	double arg = nJinn::gameTime::get() * speed;
	s = sin(arg);
	c = cos(arg);
	Eigen::Vector3d pos(s, c, 0);
	pos *= radius;
	pos += center;
	mOwner->position(pos);
}
