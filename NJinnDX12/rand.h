#pragma once

#include <random>
#include <Eigen/Dense>

namespace nJinn {
	class rand
	{
	private:
		static std::default_random_engine sEngine;
		rand() = delete;
	public:
		static double uniform(double min = 0, double max = 1);
		
		template<typename T, int c>
		static Eigen::Matrix<T, c, 1> uniform(T min = 0, T max = 1);
	};

	template<typename T, int c>
	inline Eigen::Matrix<T, c, 1> rand::uniform(T min, T max)
	{
		Eigen::Matrix<T, c, 1> ret;
		for (int i = 0; i < c; ++i) {
			ret[i] = uniform(min, max);
		}
		return ret;
	}

}