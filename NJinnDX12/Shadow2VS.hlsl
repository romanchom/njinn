struct input {
	float3 pos : POSITION;
	float4x4 viewPos : VP0;
};

struct output {
	//float3 positionDiff : POS_DIFF;
	float4 pos : SV_POSITION;
	uint rtvID : SV_RenderTargetArrayIndex;
};

#include "objectConstants.h"
#include "shadowConstants.h"
#include "rootConstants.hlsli"

output main(in input i, uint id : SV_InstanceID)
{
	output o;
	o.pos = mul(object.model, float4(i.pos, 1));
	//o.positionDiff = (o.pos - shadow.lightPos).xyz;
	o.pos = mul(i.viewPos, o.pos);
	o.rtvID = id;
	return o;
}