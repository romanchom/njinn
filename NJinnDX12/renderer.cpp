#include "stdafx.h"
#include "renderer.h"
#include "context.h"
#include "d3dx12.h"
#include "dx12helper.h"

namespace nJinn {
	std::unordered_set<renderer*> renderer::sRenderers;

	renderer::renderer(gameObject * owner) : component(owner, 101)
	{
		sRenderers.insert(this);
	}

	renderer::~renderer()
	{
		sRenderers.erase(this);
	}

	void renderer::drawAll(ID3D12GraphicsCommandList * list)
	{
		for (renderer * r : sRenderers) {
			r->draw(list);
		}
	}

	void renderer::drawDepthOnlyAll(ID3D12GraphicsCommandList * list)
	{
		for (renderer * r : sRenderers) {
			r->drawDepthOnly(list);
		}
	}

	void renderer::drawDepthOnlyGSAll(ID3D12GraphicsCommandList * list)
	{
		for (renderer * r : sRenderers) {
			r->drawDepthOnlyGS(list);
		}
	}

	void renderer::drawDepthOnlyInstancedAll(ID3D12GraphicsCommandList * list, size_t instanceCount)
	{
		for (renderer * r : sRenderers) {
			r->drawDepthOnlyInstanced(list, instanceCount);
		}
	}
}