#pragma once

#include "stopwatch.h"

namespace nJinn {
	class gameTime
	{
	private:
		static stopwatch sWatch;
		static double sPreviousTime;
		static double sCurrentTime;
		static double sDeltaTime;
		static size_t sFrameCount;


		static void update();
		friend class application;
	public:
		static double get() { return sCurrentTime; }
		static double delta() { return sDeltaTime; }
		static size_t frames() { return sFrameCount; }

	};
}
