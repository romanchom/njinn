#pragma once

#include <deque>
#include <d3d12.h>
#include <iostream>
#include "dx12helper.h"

namespace nJinn {
	class constBuff
	{
		constBuff() = delete;

		template<typename T>
		struct address{
			T * cpuP;
			uint64_t gpuP;
		};

		class allocator {
		public:
			Microsoft::WRL::ComPtr<ID3D12Resource> buffer;

			void * begin;
			void * end;
			void * current;
			void * prev[3];

			uint64_t gpuBegin;
			uint64_t gpuCurrent;
			bool lastBeforeEnd;
			uint32_t full;
			allocator(size_t size);
			~allocator();
			
			address<void> allocate(size_t length);
			
			bool makeFree();
			size_t size;
		};

		static std::deque<allocator> mAllocators;
		
	public:
		static void initialize();
		static void finalize();
		template<typename T>
		static address<T> create();
		static void free();
	};


	template<typename T>
	constBuff::address<T> constBuff::create()
	{
		size_t size = ((sizeof(T) + 255) & ~255);
		constBuff::address<void> ret;
		ret = mAllocators.back().allocate(size);
		if (ret.cpuP == nullptr) {
			DBOUT("Alloc " << mAllocators.back().size * 2 << std::endl);
			mAllocators.emplace_back(mAllocators.back().size * 2);
			return create<T>();
		}
		return reinterpret_cast<constBuff::address<T> &>(ret);
	}
}
