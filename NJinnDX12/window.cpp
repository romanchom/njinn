#include "stdafx.h"

#include "window.h"

#include "dx12helper.h"

#include "context.h"
#include "input.h"
#include "application.h"

using namespace Microsoft::WRL;

namespace nJinn {
	window::window(uint32_t width, uint32_t height) :
		mWidth(width),
		mHeight(height)
	{
		// Initialize the window class.
		WNDCLASSEX windowClass = { 0 };
		windowClass.cbSize = sizeof(WNDCLASSEX);
		windowClass.style = CS_HREDRAW | CS_VREDRAW;
		windowClass.lpfnWndProc = WindowProc;
		windowClass.hInstance = application::shInstance;
		windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		windowClass.lpszClassName = L"WindowClass1";
		RegisterClassEx(&windowClass);

		RECT windowRect = { 0, 0, width, height };
		AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

		// Create the window and store a handle to it.
		mWindowHandle = CreateWindowEx(NULL,
			L"WindowClass1",
			L"nJinn",
			WS_OVERLAPPEDWINDOW,
			100,
			50,
			windowRect.right - windowRect.left,
			windowRect.bottom - windowRect.top,
			NULL,		// We have no parent window, NULL.
			NULL,		// We aren't using menus, NULL.
			application::shInstance,
			NULL);		// We aren't using multiple windows, NULL.

		ShowWindow(mWindowHandle, application::snCmdShow);
	}

	int window::run()
	{
		MSG msg = { 0 };
		input::update();
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			input::handleMessages(msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
				return -1;
		}

		// Return this part of the WM_QUIT message to Windows.
		return 1;
	}

	void nJinn::window::title(const std::wstring & value)
	{
		SetWindowText(mWindowHandle, value.c_str());
	}

	LRESULT window::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		// Handle destroy/shutdown messages.
		switch (message)
		{
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		}

		// Handle any messages the switch statement didn't.
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

}

