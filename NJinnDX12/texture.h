#pragma once

#include <d3d12.h>
#include <memory>
#include "trackedResource.h"

namespace nJinn {
	class texture : public trackedResource<texture>
	{
	protected:
		Microsoft::WRL::ComPtr<ID3D12Resource> mResource;
		D3D12_SHADER_RESOURCE_VIEW_DESC mView;
		uint32_t mWidth;
		uint32_t mHeight;
		uint32_t mLayerCount;
	public:
		texture();
		texture(const std::wstring & fileName);
		virtual ~texture() {};
		ID3D12Resource * get() { return mResource.Get(); }
		void createShaderView(D3D12_CPU_DESCRIPTOR_HANDLE handle);
		D3D12_GPU_VIRTUAL_ADDRESS shaderResourceView() { return mResource->GetGPUVirtualAddress(); }
		uint32_t width() const { return mWidth; }
		uint32_t height() const { return mHeight; }
	};

	typedef std::shared_ptr<class texture> pTexture;
}
