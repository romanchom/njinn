#include "stdafx.h"
#include "directionalLight.h"
#include "mesh.h"
#include "context.h"
#include "gameObject.h"
#include "camera.h"
#include "geometryRootSig.h"

namespace nJinn {
	directionalLight::directionalLight(gameObject * owner) : lightSource(owner),
		mTextureViewHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, 5)
	{
		mVertexShader = shader::load({ L"DirectionalLightVS", vertex });
		mPixelShader = shader::load({ L"DirectionalLightPS", pixel });

		// TODO make it shared across all directional lights, optimize and stuff
		D3D12_GRAPHICS_PIPELINE_STATE_DESC pipe = ctx->deferredRenderer.mLigthingPSODesc;
		pipe.InputLayout = mesh::plane()->layoutDescription();
		// TODO query it from mesh
		pipe.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pipe.VS = mVertexShader->getByteCode();
		pipe.PS = mPixelShader->getByteCode();

		DC(device->CreateGraphicsPipelineState(&pipe, IID_PPV_ARGS(&mPipelineState)));

		//mConstants.color << 1, 1, 1, 1;
	}

	directionalLight * directionalLight::copyTo(gameObject * target) const
	{
		// TODO implement me
		throw std::runtime_error("not implemented");
	}

	void directionalLight::setGBufferTextures(renderTarget & rt)
	{
		rt.createShaderResourceViews(mTextureViewHeap, 0);
	}

	void directionalLight::initialize()
	{
	}

	void directionalLight::update()
	{
		Eigen::Vector4d dir(0, 0, 1, 0);
		dir = mOwner->transform() * dir;
		dir = camera::main()->viewMatrix() * dir;
		dir.normalize();
		//mConstants.direction = dir.cast<float>();
		// TODO FIXME
	}

	void directionalLight::illuminate(ID3D12GraphicsCommandList * list)
	{
		//list->SetGraphicsRootConstantBufferView(geometryRootSig::perObjectCBLocation, mConstantBuffer.GPUAddress());
		list->SetPipelineState(mPipelineState.Get());
		mesh::plane()->draw(list);
	}
	
	void directionalLight::generateShadowMap()
	{
		/*mRenderTarget.set(list);
		mRenderTarget.clear(list);
		// TODO set world constants for objects to be drawn
		lightSource::drawShadowCasters(list);
		mRenderTarget.endFrame(list);*/
	}
}