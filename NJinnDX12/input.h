#pragma once

#include <Eigen/Dense>

namespace nJinn {
	enum keyState {
		pressed = 0x1,
		released = 0x2,
		justPressed = 0x4,
		justReleased = 0x8
	};
	enum keyCode {
		mouseButtonLeft = 1,
		mouseButtonRight,
		keyBreak,
		mouseButtonMiddle,
		mouseButtonX1,
		mouseButtonX2,
		keyBackspace = 8,
		keyTab,
		keyClear = 0x0c,
		keyEnter,
		keyShift = 0x16,
		keyCtrl,
		keyPause,
		keyCapslock,
		keyEsc = 0x1b,
		keySpace = 0x20,
		keyPageUp,
		keyPageDown,
		keyEnd,
		keyHome,
		keyLeft,
		keyUp,
		keyRight,
		keyDown,
		keySelect,
		keyPrint,
		keyExecute,
		keyPrintScreen,
		keyInsert,
		keyDelete,
		keyHelp,
		key0,
		key1,
		key2,
		key3,
		key4,
		key5,
		key6,
		key7,
		key8,
		key9,
		keyA = 'A',
		keyB,
		keyC,
		keyD,
		keyE,
		keyF,
		keyG,
		keyH,
		keyI,
		keyJ,
		keyK,
		keyL,
		keyM,
		keyN,
		keyO,
		keyP,
		keyQ,
		keyR,
		keyS,
		keyT,
		keyU,
		keyV,
		keyW,
		keyX,
		keyY,
		keyZ,
		keyLeftWin,
		keyRightWin,
		keyApplication,
		keySleep = 0x5f,
		keyNum0,
		keyNum1,
		keyNum2,
		keyNum3,
		keyNum4,
		keyNum5,
		keyNum6,
		keyNum7,
		keyNum8,
		keyNum9,
		keyNumStar,
		keyNumPlus,
		keyNumComma,
		keyNumMinus,
		keyNumSlash,
		keyF1,
		keyF2,
		keyF3,
		keyF4,
		keyF5,
		keyF6,
		keyF7,
		keyF8,
		keyF9,
		keyF10,
		keyF11,
		keyF12,
		keyF13,
		keyF14,
		keyF15,
		keyF16,
		keyF18,
		keyF19,
		keyF20,
		keyF21,
		keyF22,
		keyF23,
		keyF24,
		keyNumlock = 0x90,
		keyScrollLock,
		keyLeftShift = 0xA0,
		keyRightShift,
		keyLeftCtrl,
		keyRightCtrl,
		keyLeftMenu,
		keyRightMenu
	};

	class input
	{
	private:
		struct inputState {
			uint8_t keyboardState[256];
			uint32_t mousePosX;
			int32_t mousePosY;
			int32_t mouseDeltaX;
			int32_t mouseDeltaY;
		};
		static inputState sCurrentState;
		static inputState sPreviousState;

		friend class application;
		static void update();

		friend class window;
		static void handleMessages(MSG & message);

	public:
		static bool is(keyCode code, keyState state);
		static void mousePosition(Eigen::Vector2d & position, bool pixels = false);
	};
}
