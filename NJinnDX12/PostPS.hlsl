struct input {
	float2 uv : TEX_COORD0;
};

Texture2D tex : register(t0);
sampler sampl : register(s0);

float4 main(input i) : SV_TARGET
{
	//return float4(1, 0, 0, 0);
	return tex.Sample(sampl, i.uv);
}