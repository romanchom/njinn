#pragma once

#include <d3d12.h>
#include "commandQueue.h"
#include "deferredRenderer.h"
#include "frameResources.h"
#include "profilerDisplay.h"

struct ID3D12Device;

namespace nJinn {
	class context
	{
	private:
		static void initialize(class window * win);
		static void finalize();
		static void createDevice();
		static void destroyDevice();
		static void createDebugConsole();

		class window * mWindow;
		double mGPUTimerFrequency;

		context(class window * win);
	public:
		commandQueue mainCommandQueue;
		commandQueue copyCommandQueue;

		frameResources frameResources;
		deferredRenderer deferredRenderer;
		profilerDisplay profiler;

		int frameIndex() { return frameResources.frameIndex(); }

		window * mainWindow() { return mWindow; };
		double GPUTimerFrequency() { return mGPUTimerFrequency; }

		friend class application;
	};

	extern context * ctx;
	extern ID3D12Device * device;
}
