#pragma once

namespace nJinn {
	class fullScreenQuad
	{
	private:
		static std::shared_ptr<class mesh> sMesh;
		static void draw(class ID3D12GraphicsCommandList * commandList);

		static void initialize();
		static void finalize();
	};
}
