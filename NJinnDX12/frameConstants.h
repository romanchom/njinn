#ifndef FRAMECONSTANTS_H
#define FRAMECONSTANTS_H

#include "hlslTypeDefs.h"

struct frameConstants {
	float4x4 view;
	float4x4 projection;
	float4x4 viewProjection;
	float4x4 invProjection;

	float4 viewDir;

	float2 screenPosMul;
	float2 screenPosAdd;

	float nearPlane;
	float farPlane;
	float __padd0;
	float __padd1;
};

#ifndef _WIN32
ConstantBuffer<frameConstants> frame : register(b0);
#endif

#endif