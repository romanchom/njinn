#pragma once

#include <vector>
#include <Eigen/Dense>

struct ID3D12RootSignature;
#include "bufferedCommandList.h"
#include "renderTarget.h"
#include "threadPool.h"
#include "renderingTask.h"
#include "gpuStopwatch.h"

namespace nJinn {
	class deferredRenderer
	{
		class gBuffer : public renderTarget {
		private:
			descriptorHeap mTextureViewHeap;
		public:
			template<typename ...Args>
			gBuffer(Args...args) : renderTarget(args...), mTextureViewHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, sizeof...(args) + 1) {};
			pTexture depth() { return mDepthStencliTexture; }
			pTexture albedo() { return mRenderTextures[0]; }
			pTexture normal() { return mRenderTextures[1]; }
			void initialize();

			static const DXGI_FORMAT depthStencilFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
			static const DXGI_FORMAT albedoAlphaFormat  = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
			static const DXGI_FORMAT normalFormat = DXGI_FORMAT_R16G16_SNORM;
		};

		class geometryPassTask : public renderingTask {
		public:
			geometryPassTask();
			virtual void execute() override;
		};

		class lightPassTask : public renderingTask {
		public:
			lightPassTask();
			virtual void execute() override;
		};
	public:
		deferredRenderer();
		~deferredRenderer();

		void draw();
		D3D12_GRAPHICS_PIPELINE_STATE_DESC mGeometryPSODesc;
		D3D12_GRAPHICS_PIPELINE_STATE_DESC mLigthingPSODesc;
		D3D12_GRAPHICS_PIPELINE_STATE_DESC mShadowPSODesc;
		D3D12_GRAPHICS_PIPELINE_STATE_DESC mPostPSODesc;
		gBuffer mGBuffer;

		void submitRenderingTask(renderingTask * t);
		Microsoft::WRL::ComPtr<ID3D12RootSignature> mRootSignature;
	private:
		void createRootSignature();
		void createPartialPSO();

		threadPool mThreadPool;
		std::vector<ID3D12CommandList *> mCommandListQueue;

		friend class context;
		geometryPassTask geometryTask;
		lightPassTask lightTask;
		void geometryPass(ID3D12GraphicsCommandList * list);
		void lightPass(ID3D12GraphicsCommandList * list);

		uint64_t mFrameConstantsAddress;

		gpuStopwatch geometryPassStopWatch;
	};
}
