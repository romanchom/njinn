#include "stdafx.h"
#include "shader.h"
#include "config.h"
#include <fstream>
#include <d3dcompiler.h>
#include "dx12helper.h"

namespace nJinn {
	shader::shader(const shaderDesc & desc)
	{
		std::fstream file;
		std::wstring prefix = config::assetPath() + desc.name;
		std::wstring filePath = prefix + L".cso";
		file.open(filePath, std::fstream::in | std::fstream::binary);
		if (file.good()) {
			file.seekg(0, std::ios::end);
			size_t size = (size_t)file.tellg();
			file.seekg(0, std::ios::beg);

			DC(D3DCreateBlob(size, &byteCode));

			byteCodeStruct.BytecodeLength = size;
			byteCodeStruct.pShaderBytecode = byteCode->GetBufferPointer();

			file.read((char*)(byteCodeStruct.pShaderBytecode), size);
		}
		else {
			filePath = prefix + L".hlsl";

			UINT compileFlags = 0;
#ifdef _DEBUG
			compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
			static const char * shaderVersions[] = {
				"vs_5_0",
				"ps_5_0",
				"gs_5_0"
			};

			DC(D3DCompileFromFile(filePath.c_str(), nullptr, nullptr, "main", shaderVersions[desc.type], compileFlags, 0, &byteCode, nullptr));

			byteCodeStruct.BytecodeLength = byteCode->GetBufferSize();
			byteCodeStruct.pShaderBytecode = byteCode->GetBufferPointer();
		}
	}
}