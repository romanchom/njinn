#include "stdafx.h"
#include "deferredRenderer.h"

#include <d3d12.h>
#include <comdef.h>
#include "dx12helper.h"
#include "context.h"
#include "window.h"
#include "camera.h"
#include "resourceAllocator.h"
#include "renderer.h"
#include "lightSource.h"
#include "geometryRootSig.h"
#include "constBuff.h"
#include "frameConstants.h"

using namespace Eigen;

namespace nJinn {
	float albedoClear[4] = { 0, 0, 0, 0 };
	float normalClear[4] = { 0, 0, 0, 0 };
	const uint32_t width = 1600;
	const uint32_t height = 900;
	deferredRenderer::deferredRenderer() :
		mGBuffer(
			std::make_shared<depthStencilTexture>(width, height, gBuffer::depthStencilFormat, 0.0f, 0),
			std::make_shared<renderTexture>(width, height, gBuffer::albedoAlphaFormat, albedoClear),
			std::make_shared<renderTexture>(width, height, gBuffer::normalFormat, normalClear)),
		mThreadPool(config::threadCount()),
		geometryPassStopWatch(2)
	{
		createRootSignature();
		createPartialPSO();
		mGBuffer.initialize();
		constBuff::initialize();
	}

	deferredRenderer::~deferredRenderer()
	{
		constBuff::finalize();
	}

	void deferredRenderer::draw()
	{
		constBuff::free();
		resourceAllocator::executeUpload();

		submitRenderingTask(&geometryTask);
		lightSource::drawAllShadowMaps();
		submitRenderingTask(&lightTask);

		mThreadPool.waitUntillCompleted();

		ctx->mainCommandQueue->ExecuteCommandLists(mCommandListQueue.size(), &mCommandListQueue[0]);
		mCommandListQueue.clear();
		ctx->profiler.draw();
	}

	void deferredRenderer::submitRenderingTask(renderingTask * t)
	{
		t->mCommandList.reset();
		mCommandListQueue.push_back(t->mCommandList);
		mThreadPool.submitTask(t);
	}

	void deferredRenderer::createRootSignature()
	{
		CD3DX12_STATIC_SAMPLER_DESC staticSamplers[geometryRootSig::staticSamplerCount];
		CD3DX12_DESCRIPTOR_RANGE ranges[2];
		CD3DX12_ROOT_PARAMETER rootParameters[geometryRootSig::rootSigParamCount];

		ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 5, 0);
		ranges[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 5, geometryRootSig::staticSamplerCount);

		rootParameters[geometryRootSig::perFrameCBLocation].InitAsConstantBufferView(0); // per frame constants
		rootParameters[geometryRootSig::perObjectCBLocation].InitAsConstantBufferView(1); // per object constants
		rootParameters[geometryRootSig::shadowCBLocation].InitAsConstantBufferView(2); // per object constants

		rootParameters[geometryRootSig::perObjectTexturesLocation].InitAsDescriptorTable(1, ranges); // per object textures
		rootParameters[geometryRootSig::perObjectSamplersLocation].InitAsDescriptorTable(1, ranges + 1); // per object samplers
		rootParameters[geometryRootSig::index0].InitAsConstants(1, 3); // global index0


		staticSamplers[0].Init(0, D3D12_FILTER_MIN_MAG_MIP_LINEAR);
		staticSamplers[1].Init(1, D3D12_FILTER_MIN_MAG_MIP_POINT);
		staticSamplers[2].Init(2, D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT);
		staticSamplers[2].ComparisonFunc = D3D12_COMPARISON_FUNC_GREATER;

		D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
			D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;

		CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
		rootSignatureDesc.Init(_countof(rootParameters), rootParameters, geometryRootSig::staticSamplerCount, staticSamplers, rootSignatureFlags);

		Microsoft::WRL::ComPtr<ID3DBlob> signature;
		Microsoft::WRL::ComPtr<ID3DBlob> error;
		if (FAILED((D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error)))) {
			DBOUT((char *) error.Get()->GetBufferPointer());
			throw;
		}

		DC(device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&mRootSignature)));
	}

	void deferredRenderer::createPartialPSO()
	{
		mShadowPSODesc = {};

		mShadowPSODesc.pRootSignature = mRootSignature.Get();

		mShadowPSODesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
		mShadowPSODesc.RasterizerState.CullMode = D3D12_CULL_MODE_BACK;
		mShadowPSODesc.RasterizerState.FrontCounterClockwise = true;

		mShadowPSODesc.SampleMask = UINT_MAX;
		mShadowPSODesc.SampleDesc.Count = 1;

		mShadowPSODesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		mShadowPSODesc.DepthStencilState.DepthEnable = true;
		mShadowPSODesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_GREATER;
		mShadowPSODesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		mShadowPSODesc.DepthStencilState.StencilEnable = false;
		mShadowPSODesc.NumRenderTargets = 0;
		mShadowPSODesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);


		mLigthingPSODesc = mShadowPSODesc;
		//mShadowPSODesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;

		mLigthingPSODesc.NumRenderTargets = 1;
		mLigthingPSODesc.DSVFormat = gBuffer::depthStencilFormat;
		mLigthingPSODesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;

		mGeometryPSODesc = mLigthingPSODesc;

		D3D12_RENDER_TARGET_BLEND_DESC blend;
		blend.BlendEnable = true;
		blend.LogicOpEnable = false;
		blend.BlendOp = D3D12_BLEND_OP_ADD;
		blend.DestBlend = D3D12_BLEND_ONE;
		blend.SrcBlend = D3D12_BLEND_SRC_ALPHA;
		blend.BlendOpAlpha = D3D12_BLEND_OP_MAX;
		blend.DestBlendAlpha = D3D12_BLEND_ONE;
		blend.SrcBlendAlpha = D3D12_BLEND_ONE;
		blend.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		mLigthingPSODesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS;
		mLigthingPSODesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
		mLigthingPSODesc.BlendState.IndependentBlendEnable = false;
		mLigthingPSODesc.BlendState.AlphaToCoverageEnable = false;
		for (int i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i) {
			mLigthingPSODesc.BlendState.RenderTarget[i] = blend;
		}

		mGeometryPSODesc.NumRenderTargets = 2;
		mGeometryPSODesc.RTVFormats[0] = gBuffer::albedoAlphaFormat;
		mGeometryPSODesc.RTVFormats[1] = gBuffer::normalFormat;

		mPostPSODesc = mLigthingPSODesc;
	}

	void deferredRenderer::geometryPass(ID3D12GraphicsCommandList * list)
	{
		auto cbuff = constBuff::create<frameConstants>();
		frameConstants & fc = *cbuff.cpuP;
		mFrameConstantsAddress = cbuff.gpuP;

		geometryPassStopWatch.reset(list);
		geometryPassStopWatch.measure(0, list);
		fc.view = camera::main()->viewMatrix().cast<float>();
		fc.projection = camera::main()->projectionMatrix().cast<float>();
		fc.viewProjection = camera::main()->viewProjectionMatrix().cast<float>();
		fc.invProjection = camera::main()->projectionMatrix().inverse().cast<float>();

		fc.nearPlane = camera::main()->nearPlane();
		fc.farPlane = camera::main()->farPlane();
		Vector4d viewDir2 = camera::main()->projectionMatrix().inverse() * Eigen::Vector4d(1, 1, 0, 1);
		viewDir2 /= viewDir2.w();
		fc.viewDir = (viewDir2).cast<float>();

		double t = tan(0.5 * camera::main()->fieldOfView());
		Vector2d screenPosMul(2 * t, -2 * t);
		Vector2d screenDim = ctx->mainWindow()->dimentions();
		screenPosMul /= screenDim.y();

		Vector2d screenPosAdd(-screenDim.x() * t / screenDim.y(), t);

		fc.screenPosMul = screenPosMul.cast<float>();
		fc.screenPosAdd = screenPosAdd.cast<float>();


		list->SetGraphicsRootSignature(ctx->deferredRenderer.mRootSignature.Get());
		list->SetGraphicsRootConstantBufferView(0, mFrameConstantsAddress);

		mGBuffer.set(list);
		mGBuffer.clear(list);
		renderer::drawAll(list);
		mGBuffer.endFrame(list);
		geometryPassStopWatch.measure(1, list);
	}

	void deferredRenderer::lightPass(ID3D12GraphicsCommandList * list)
	{
		list->SetGraphicsRootSignature(mRootSignature.Get());
		list->SetGraphicsRootConstantBufferView(0, mFrameConstantsAddress);
		ctx->frameResources.setAsRenderTarget(list);
		float colors[] = { 0, 0, 0, 0 };
		ctx->frameResources.clear(list, colors);
		lightSource::drawAll(list);
		//ctx->frameResources.endFrame(list);
	}

	void deferredRenderer::gBuffer::initialize()
	{
		albedo()->createShaderView(mTextureViewHeap.getCPUHandle(0));
		normal()->createShaderView(mTextureViewHeap.getCPUHandle(1));
		depth()->createShaderView(mTextureViewHeap.getCPUHandle(2));
	}

	deferredRenderer::geometryPassTask::geometryPassTask()
	{
		SET_NAME(mCommandList, L"Geo pass command list");
	}

	void deferredRenderer::geometryPassTask::execute()
	{
		ctx->deferredRenderer.geometryPass(mCommandList);
		mCommandList.close();
	}

	deferredRenderer::lightPassTask::lightPassTask()
	{
		SET_NAME(mCommandList, L"Light pass command list");
	}

	void deferredRenderer::lightPassTask::execute()
	{
		ctx->deferredRenderer.lightPass(mCommandList);
		mCommandList.close();
	}
}