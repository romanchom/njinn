#pragma once

#include <d3d12.h>

namespace nJinn {
	class descriptorHeap
	{
	private:
		size_t mSize;
		D3D12_DESCRIPTOR_HEAP_TYPE mType;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> mDescriptorHeap;
		size_t mIncrement;
	public:
		descriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE type, size_t initialSize = 64);
		D3D12_CPU_DESCRIPTOR_HANDLE getCPUHandle(size_t index = 0);
		D3D12_GPU_DESCRIPTOR_HANDLE getGPUHandle(size_t index = 0);
		ID3D12DescriptorHeap * get() { return mDescriptorHeap.Get(); }
		size_t size() const { return mSize; }

		operator ID3D12DescriptorHeap * () {
			return mDescriptorHeap.Get();
		}
	};
}
