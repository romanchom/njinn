#include "stdafx.h"
#include "commandQueue.h"

#include "context.h"
#include "dx12helper.h"

namespace nJinn {
	commandQueue::commandQueue(D3D12_COMMAND_LIST_TYPE type, int priority, D3D12_COMMAND_QUEUE_FLAGS flags, unsigned nodeMask) {
		D3D12_COMMAND_QUEUE_DESC queueDesc;
		queueDesc.Flags = flags;
		queueDesc.Type = type;
		queueDesc.NodeMask = nodeMask;
		queueDesc.Priority = priority;

		DC(device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&mQueue)));
	}
}