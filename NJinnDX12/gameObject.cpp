#include "stdafx.h"
#include "gameObject.h"

#include "context.h"

namespace nJinn {
	using namespace Eigen;

	gameObject::set_type gameObject::sGameObjects;

	gameObject::gameObject() :
		mParent(nullptr),
		mTransformChanged(true)
	{
		mPosition.setZero();
		mScale.setOnes();
		mRotation.setIdentity();
		mTransform.setIdentity();
	}

	gameObject::gameObject(const gameObject & original) :
		mParent(nullptr),
		mTransformChanged(true),
		mPosition(original.mPosition),
		mScale(original.mScale),
		mRotation(original.mRotation)
	{
		parent(original.mParent);

		for (component * c : original.mComponents) {
			mComponents.push_back(c->copyTo(this));
		}

		for (gameObject * go : original.mChildren) {
			go->copy();
		}
	}

	gameObject::~gameObject()
	{
		for (component * c : mComponents) delete c;
	}

	gameObject * gameObject::create()
	{
		auto it = sGameObjects.emplace();
		auto ret = &*(it.first);
		// unordered_set contains immutable objects, 
		// but since we know that hash depends on object location in memory
		// we can safely cast constness away
		return (gameObject *) ret;
	}

	gameObject * gameObject::addChild(){
		gameObject * child = gameObject::create();
		child->parent(this);
		return child;
	}

	void gameObject::parent(gameObject * value)
	{
		if (mParent) {
			auto & vec = mParent->mChildren;
			auto it = std::find(vec.begin(), vec.end(), this);
			assert(it != vec.end());
			size_t index = it - vec.begin();
			if (index > vec.size() - 1) {
				vec[index] = vec.back();
			}
			vec.pop_back();
		}
		mParent = value;
		markTransformChanged();
		if(mParent) {
			mParent->mChildren.push_back(this);
		}
	}

	void gameObject::destroyNow()
	{
		sGameObjects.erase(*this);
	}

	gameObject * gameObject::copy()
	{
		auto it = sGameObjects.emplace(*this);
		auto ret = &*(it.first);
		// unordered_set contains immutable objects, 
		// but since we know that hash depends on object location in memory
		// we can safely cast constness away
		return (gameObject *)ret;
	}

	void gameObject::finalize()
	{
		sGameObjects.clear();
	}

	const Matrix4d & gameObject::transform(){
		if (mTransformChanged) {
			Affine3d t;
			t.setIdentity();
			t.translate(mPosition);
			t.rotate(mRotation);
			t.scale(mScale);
			mTransform = t.matrix();
			if (mParent) mTransform *= mParent->transform();
			mTransformChanged = false;
		}
		return mTransform;
	}
}