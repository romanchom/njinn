#include "NormalCodec.hlsli"
#include "DepthDecode.hlsli"

struct input {
	float4 pos : SV_POSITION;
	float3 viewDir : VIEW_DIR;
};

cbuffer frameConstants : register(b0)
{
	float4x4 view;
	float4x4 projection;
	float4x4 viewProjection;
	float4x4 invProjection;
	float4 viewDir;
	float nearPlane;
	float farPlane;
};

cbuffer objectConstants : register(b1)
{
	float4 color;
	float4 direction;
}

Texture2D albedoTex : register(t0);
Texture2D<float2> normalTex : register(t1);
Texture2D<float> depthTex : register(t2);
sampler mipSampler : register(s0);
sampler pointSampler : register(s1);

float4 main(input i, float4 screenPos : SV_POSITION) : SV_TARGET
{
	int3 uv = (int3) screenPos.xyz;
	float4 albedo = albedoTex.Load(uv);
	float3 normal = decodeNormal(normalTex.Load(uv));
	float depth = decodeDepth01Exact(depthTex.Load(uv), nearPlane, farPlane);
	float3 viewDir = normalize(i.viewDir);

	float3 diffuse = albedo.xyz * max(0, dot(normal, direction.xyz));
	float3 reflectedLight = reflect(direction.xyz, normal);
	float3 specular = max(0, dot(reflectedLight, -viewDir));
	specular = pow(specular, 100);
	return float4(color * (specular + diffuse), albedo.w);
}