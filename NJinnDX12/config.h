#pragma once

#include <string>
#include <boost/program_options.hpp>


namespace nJinn {
	class config {
	private:
		static std::wstring mAssetPath;
	public:
		static const unsigned int backBufferCount = 3;
		static void initialize(const char * args);
		static const std::wstring& assetPath() {
			return mAssetPath;
		}
		static uint32_t shadowMapResolution();
		static uint32_t threadCount();

		static boost::program_options::options_description optionsDesc;
		static boost::program_options::variables_map options;
	};
}