#pragma once

#include <d3d12.h>

namespace nJinn {
	class gpuStopwatch
	{
	private:
		size_t mQueryCount;
		size_t mCurrent;
		uint64_t * mResults;
		uint64_t * mCPUResults;
		double mCPUFrequency;
		Microsoft::WRL::ComPtr<ID3D12QueryHeap> mQueryHeap;
		Microsoft::WRL::ComPtr<ID3D12Resource> mDestinationBuffer;
	public:
		gpuStopwatch(size_t measurementCount);
		~gpuStopwatch();
		void measure(size_t slot, ID3D12GraphicsCommandList * list);
		double getDifference(size_t begin, size_t end);
		double getCPUDifference(size_t begin, size_t end);
		void reset(ID3D12GraphicsCommandList * list);
	};
}
