#pragma once

class stopwatch
{
public:
	stopwatch();

	void reset();
	double seconds();
private:
	LARGE_INTEGER mFrequency;
	LARGE_INTEGER mStart;
};

