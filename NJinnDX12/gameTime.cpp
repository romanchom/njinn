#include "stdafx.h"
#include "gameTime.h"

namespace nJinn {
	stopwatch gameTime::sWatch;

	double gameTime::sPreviousTime = 0;
	double gameTime::sCurrentTime = 0;
	double gameTime::sDeltaTime = 0;
	size_t gameTime::sFrameCount = 0;

	void gameTime::update()
	{
		sPreviousTime = sCurrentTime;
		sCurrentTime = sWatch.seconds();
		sDeltaTime = sCurrentTime - sPreviousTime;
		++sFrameCount;
	}
}