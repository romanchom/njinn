#include "stdafx.h"
#include "commandBundle.h"

#include "context.h"
#include "dx12helper.h"

namespace nJinn {
	ID3D12CommandAllocator * commandBundle::sCommandAllocator;

	ID3D12GraphicsCommandList * commandBundle::beginRecording(ID3D12PipelineState * initialPipelineState)
	{
		DC(device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_BUNDLE, sCommandAllocator, initialPipelineState, IID_PPV_ARGS(&mCommandBundle)));
		return mCommandBundle.Get();
	}

	void commandBundle::initialize()
	{
		DC(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, IID_PPV_ARGS(&sCommandAllocator)));
	}

	void commandBundle::finalize()
	{
		sCommandAllocator->Release();
	}
}