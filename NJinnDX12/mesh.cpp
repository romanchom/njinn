#include "stdafx.h"
#include "mesh.h"
#include "meshData.h"

#include "dx12helper.h"

#include "context.h"
#include "resourceAllocator.h"

namespace nJinn {
	pMesh mesh::sPlane = nullptr;

	mesh::mesh(const std::wstring & fileName)
	{
		loadSbm(config::assetPath() + fileName);

		SET_NAME(mVertexBuffer, L"Mesh vertex buffer");
	}

	const char * semantics[] = {
		"POSITION",
		"TEX_COORD",
		"NORMAL",
		"TANGENT",
	};

	DXGI_FORMAT formatTable[10][4] = {
		{ DXGI_FORMAT_R8_SINT,		DXGI_FORMAT_R8G8_SINT,		DXGI_FORMAT_R8G8B8A8_SINT,		DXGI_FORMAT_R8G8B8A8_SINT },// BYTE
		{ DXGI_FORMAT_R8_UINT,		DXGI_FORMAT_R8G8_UINT,		DXGI_FORMAT_R8G8B8A8_UINT,		DXGI_FORMAT_R8G8B8A8_UINT },// UNSIGNED_BYTE
		{ DXGI_FORMAT_R16_SINT,		DXGI_FORMAT_R16G16_SINT,	DXGI_FORMAT_R16G16B16A16_SINT,	DXGI_FORMAT_R16G16B16A16_SINT },// SHORT
		{ DXGI_FORMAT_R16_UINT,		DXGI_FORMAT_R16G16_UINT,	DXGI_FORMAT_R16G16B16A16_UINT,	DXGI_FORMAT_R16G16B16A16_UINT },// UNSIGNED_SHORT
		{ DXGI_FORMAT_R16_FLOAT,	DXGI_FORMAT_R16G16_FLOAT,	DXGI_FORMAT_R16G16B16A16_FLOAT,	DXGI_FORMAT_R16G16B16A16_FLOAT },// HALF_FLOAT
		{ DXGI_FORMAT_R32_FLOAT,	DXGI_FORMAT_R32G32_FLOAT,	DXGI_FORMAT_R32G32B32_FLOAT,	DXGI_FORMAT_R32G32B32A32_FLOAT },// FLOAT
		{ DXGI_FORMAT_R32_SINT,		DXGI_FORMAT_R32G32_SINT,	DXGI_FORMAT_R32G32B32_SINT,		DXGI_FORMAT_R32G32B32A32_SINT },// INT
		{ DXGI_FORMAT_R32_UINT,		DXGI_FORMAT_R32G32_UINT,	DXGI_FORMAT_R32G32B32_UINT,		DXGI_FORMAT_R32G32B32A32_UINT },// UNSIGNED_INT
		{ DXGI_FORMAT_R32_TYPELESS,	DXGI_FORMAT_R32G32_TYPELESS,DXGI_FORMAT_R32G32B32_TYPELESS,	DXGI_FORMAT_R32G32B32A32_TYPELESS },// FIXED
		{},// DOUBLE
	};

	DXGI_FORMAT formatTableNorm[10][4] = {
		{ DXGI_FORMAT_R8_SNORM,			DXGI_FORMAT_R8G8_SNORM,			DXGI_FORMAT_R8G8B8A8_SNORM,			DXGI_FORMAT_R8G8B8A8_SNORM },// BYTE
		{ DXGI_FORMAT_R8_UNORM,			DXGI_FORMAT_R8G8_UNORM,			DXGI_FORMAT_R8G8B8A8_UNORM,			DXGI_FORMAT_R8G8B8A8_UNORM },// UNSIGNED_BYTE
		{ DXGI_FORMAT_R16_SNORM,		DXGI_FORMAT_R16G16_SNORM,		DXGI_FORMAT_R16G16B16A16_SNORM,		DXGI_FORMAT_R16G16B16A16_SNORM },// SHORT
		{ DXGI_FORMAT_R16_UNORM,		DXGI_FORMAT_R16G16_UNORM,		DXGI_FORMAT_R16G16B16A16_UNORM,		DXGI_FORMAT_R16G16B16A16_UNORM },// UNSIGNED_SHORT
		{ DXGI_FORMAT_R16_FLOAT,		DXGI_FORMAT_R16G16_FLOAT,		DXGI_FORMAT_R16G16B16A16_FLOAT,		DXGI_FORMAT_R16G16B16A16_FLOAT },// HALF_FLOAT
		{ DXGI_FORMAT_R32_FLOAT,		DXGI_FORMAT_R32G32_FLOAT,		DXGI_FORMAT_R32G32B32_FLOAT,		DXGI_FORMAT_R32G32B32A32_FLOAT },// FLOAT
		{ DXGI_FORMAT_R32_TYPELESS,		DXGI_FORMAT_R32G32_TYPELESS,	DXGI_FORMAT_R32G32B32_TYPELESS,		DXGI_FORMAT_R32G32B32A32_TYPELESS },// INT
		{ DXGI_FORMAT_R32_TYPELESS,		DXGI_FORMAT_R32G32_TYPELESS,	DXGI_FORMAT_R32G32B32_TYPELESS,		DXGI_FORMAT_R32G32B32A32_TYPELESS },// UNSIGNED_INT
		{ DXGI_FORMAT_R32_TYPELESS,		DXGI_FORMAT_R32G32_TYPELESS,	DXGI_FORMAT_R32G32B32_TYPELESS,		DXGI_FORMAT_R32G32B32A32_TYPELESS },// FIXED
		{ },// DOUBLE
	};

	static DXGI_FORMAT getFormat(int componentCount, uint32_t type, bool normalized) {
		if (normalized)
			return formatTableNorm[type][componentCount];
		else
			return formatTable[type][componentCount];
	}

	void mesh::loadSbm(const std::wstring & fileName)
	{
		meshData data;
		data.load(fileName);


		mAttributeCount = data.properties.vertexAttributeCount;
		for (int i = 0; i < data.properties.vertexAttributeCount; ++i) {
			meshData::VertexAttribute & src = data.vertexAttributes[i];
			D3D12_INPUT_ELEMENT_DESC & dst = mAttributes[i];
			dst.SemanticIndex = 0;
			dst.SemanticName = semantics[i];
			dst.AlignedByteOffset = src.offset;
			dst.Format = getFormat(src.componentCount, src.type, (bool) (src.normalized));
			dst.InputSlot = 0;
			dst.InputSlotClass = D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;
			dst.InstanceDataStepRate = 0;
		}
		mIndexCount = data.indiciesCount();


		size_t totalSize = data.indexDataSize() + data.vertexDataSize();

		resourceAllocator::allocate(mVertexBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER | D3D12_RESOURCE_STATE_INDEX_BUFFER, totalSize, data.data);

		// Initialize the vertex buffer view.
		mVertexBufferView.BufferLocation = mVertexBuffer->GetGPUVirtualAddress();
		mVertexBufferView.StrideInBytes = data.vertexSize();
		mVertexBufferView.SizeInBytes = data.vertexDataSize();

		mIndexBufferView.BufferLocation = mVertexBufferView.BufferLocation + mVertexBufferView.SizeInBytes;
		mIndexBufferView.SizeInBytes = data.indexDataSize();
		mIndexBufferView.Format = (data.indiciesCount() < (1 << 16)) ? DXGI_FORMAT_R16_UINT : DXGI_FORMAT_R32_UINT;

	}

	D3D12_INPUT_LAYOUT_DESC mesh::layoutDescription()
	{
		return { mAttributes, mAttributeCount };
	}

	void mesh::draw(ID3D12GraphicsCommandList * commandList, size_t indexCount)
	{
		commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		commandList->IASetVertexBuffers(0, 1, &mVertexBufferView);
		commandList->IASetIndexBuffer(&mIndexBufferView);
		commandList->DrawIndexedInstanced(mIndexCount, indexCount, 0, 0, 0);
	}

	void mesh::fillPSO(D3D12_GRAPHICS_PIPELINE_STATE_DESC & descriptor) const
	{
		descriptor.InputLayout = { mAttributes, mAttributeCount };
		descriptor.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	}

	pMesh mesh::plane()
	{
		return sPlane;
	}

	void mesh::initialize()
	{
		sPlane = std::make_shared<mesh>();
		sPlane->makePlane();

	}

	void mesh::finalize()
	{
		sPlane.reset();
	}

	void mesh::makePlane()
	{
		mAttributeCount = 3;
		D3D12_INPUT_ELEMENT_DESC elemDesc;
		elemDesc.SemanticIndex = 0;
		elemDesc.InstanceDataStepRate = 0;
		elemDesc.InputSlot = 0;
		elemDesc.InputSlotClass = D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA;

		elemDesc.SemanticName = "POSITION";
		elemDesc.AlignedByteOffset = 0;
		elemDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT;

		mAttributes[0] = elemDesc;

		elemDesc.SemanticName = "NORMAL";
		elemDesc.AlignedByteOffset = 12;

		mAttributes[1] = elemDesc;

		elemDesc.SemanticName = "TEX_COORD";
		elemDesc.AlignedByteOffset = 24;
		elemDesc.Format = DXGI_FORMAT_R32G32_FLOAT;

		mAttributes[2] = elemDesc;

		const float vertexData[] = {
			-1, -1, 0, 0, 0, -1, 0, 1,
			1, -1, 0, 0, 0, -1, 1, 1,
			-1,  1, 0, 0, 0, -1, 0, 0,
			1,  1, 0, 0, 0, -1, 1, 0,
		};

		const uint16_t indexData[] = {
			0, 1, 2,
			3, 2, 1
		};

		const size_t vds = sizeof(vertexData), ids = sizeof(indexData);

		char data[vds + ids];
		memcpy(data, vertexData, vds);
		memcpy(data + vds, indexData, ids);

		resourceAllocator::allocate(mVertexBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER | D3D12_RESOURCE_STATE_INDEX_BUFFER, vds + ids, data);

		mVertexBufferView.BufferLocation = mVertexBuffer->GetGPUVirtualAddress();
		mVertexBufferView.StrideInBytes = 8 * 4;
		mVertexBufferView.SizeInBytes = vds;

		mIndexBufferView.BufferLocation = mVertexBufferView.BufferLocation + mVertexBufferView.SizeInBytes;
		mIndexBufferView.SizeInBytes = vds;
		mIndexBufferView.Format = DXGI_FORMAT_R16_UINT;

		mIndexCount = 6;
	}
}