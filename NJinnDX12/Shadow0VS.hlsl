struct input {
	float3 pos : POSITION;
};

struct output {
	//float3 positionDiff : POS_DIFF;
	float4 pos : SV_POSITION;
};

#include "objectConstants.h"
#include "shadowConstants.h"
#include "rootConstants.hlsli"

output main(in input i)
{
	output o;
	o.pos = mul(object.model, float4(i.pos, 1));
	//o.positionDiff = (o.pos - shadow.lightPos).xyz;
	o.pos = mul(shadow.viewProjection[root.index0], o.pos);
	return o;
}