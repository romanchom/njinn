#include "stdafx.h"
#include "texture.h"
#include "config.h"
#include "memMapFile.h"
#include "dx12helper.h"
#include "context.h"
#include "resourceAllocator.h"

enum pixelFormatFlags {
	DDPF_ALPHAPIXELS = 0x1,
	DDPF_ALPHA = 0x2,
	DDPF_FOURCC	= 0x4,
	DDPF_RGB = 0x40,
	DDPF_YUV = 0x200,
	DDPF_LUMINANCE = 0x20000
};

const uint32_t DX10_FOURCC = 'D' | 'X' << 8 | '1' << 16 | '0' << 24;
const uint32_t DXT5_FOURCC = 'D' | 'X' << 8 | 'T' << 16 | '5' << 24;

struct ddsPixelFormat {
	uint32_t Size;
	uint32_t Flags;
	uint32_t FourCC;
	uint32_t RGBBitCount;
	uint32_t RBitMask;
	uint32_t GBitMask;
	uint32_t BBitMask;
	uint32_t ABitMask;
};

struct ddsHeader  {
	uint32_t magic;
	uint32_t size;
	uint32_t flags;
	uint32_t height;
	uint32_t width;
	uint32_t pitchOrLinearSize;
	uint32_t depth;
	uint32_t mipMapCount;
	uint32_t reserved1[11];
	ddsPixelFormat ddspf;
	uint32_t caps;
	uint32_t caps2;
	uint32_t caps3;
	uint32_t caps4;
	uint32_t reserved2;
};

struct ddsHeaderExt {
	DXGI_FORMAT dxgiFormat;
	D3D12_RESOURCE_DIMENSION resourceDimension;
	uint32_t miscFlag;
	uint32_t arraySize;
	uint32_t miscFlags2;
};

using namespace Microsoft::WRL;

namespace nJinn {
	texture::texture() :
		mResource(nullptr),
		mView()
	{
	}

	texture::texture(const std::wstring & fileName)
	{
		std::wstring name = config::assetPath() + fileName;
		
		win::memMapFile file(name);

		ddsHeader * header = (ddsHeader *) file.get();
		if (header->magic != 0x20534444) throw std::runtime_error("Incorrect file format.");

		mWidth = header->width;
		mHeight = header->height;

		ddsPixelFormat * pixelFormat = &header->ddspf;
		if (0 != (pixelFormat->Flags & DDPF_FOURCC)) {
			ComPtr<ID3D12Resource> temp;
			char * pixels = (char *)file.get(sizeof(ddsHeader));

			switch (pixelFormat->FourCC) {
			case DXT5_FOURCC:
				D3D12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Tex2D(
					DXGI_FORMAT_BC3_UNORM_SRGB,
					header->width,
					header->height,
					1,
					header->mipMapCount);
				

				resourceAllocator::allocate(mResource, desc, D3D12_RESOURCE_STATE_GENERIC_READ, temp);

				for (int i = 0; i < header->mipMapCount; ++i) {
					uint32_t width = std::max(1u, ((header->width >> i) + 3) / 4);
					uint32_t height = std::max(1u, ((header->height >> i) + 3) / 4);
					uint32_t size = width * height * 16;
					temp->WriteToSubresource(i, nullptr, pixels, width * 16, 0);
					pixels += size;
				}

				mView = {};

				mView.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
				mView.Format = DXGI_FORMAT_BC3_UNORM_SRGB;
				mView.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
				mView.Texture2D.MipLevels = header->mipMapCount;
				

				break;
			default:
				throw std::runtime_error("Unsupported format");
			}
		}
		else {
			throw std::runtime_error("Unsupported format");
		}
	}

	void texture::createShaderView(D3D12_CPU_DESCRIPTOR_HANDLE handle)
	{
		device->CreateShaderResourceView(mResource.Get(), &mView, handle);
	}
}