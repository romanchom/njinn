#pragma once

#include <d3d12.h>

namespace nJinn {
	class commandBundle
	{
	private:
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> mCommandBundle;

		static ID3D12CommandAllocator * sCommandAllocator;

		static void initialize();
		static void finalize();

		friend class nJinnDevice;
		friend class context;
	public:
		void endRecording() { mCommandBundle->Close(); }
		ID3D12GraphicsCommandList * beginRecording(ID3D12PipelineState * initialPipelineState);
		ID3D12GraphicsCommandList * get() { return mCommandBundle.Get(); }
	};
}
