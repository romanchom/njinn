#include "stdafx.h"
#include "pointLight.h"

#include <limits>

#include "dx12helper.h"
#include "context.h"
#include "gameObject.h"
#include "camera.h"
#include "geometryRootSig.h"
#include "renderTarget.h"
#include "mathGeometry.h"
#include "constBuff.h"
#include "shadowConstants.h"
#include "pointLightConstants.h"

using namespace Eigen;
namespace nJinn{
	pointLight::pointLight(gameObject * owner) :
		lightSource(owner),
		mShadowMap(std::make_shared<depthStencilTexture>(config::shadowMapResolution(), DXGI_FORMAT_D32_FLOAT, 0.0f, 0)),
		mTextureViewHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, 6),
		mColor(0, 0, 0, 0)
	{
		mMesh = mesh::load(L"sphere.sbm");
		mVertexShader = shader::load({ L"PointLightVS", vertex });
		mPixelShader = shader::load({ L"PointLightPS", pixel });
		mShadowMap->createShaderView(mTextureViewHeap.getCPUHandle());
	}

	void pointLight::initialize()
	{
		lightSource::initialize();
		D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = ctx->deferredRenderer.mLigthingPSODesc; // TODO accessor
		psoDesc.RasterizerState.CullMode = D3D12_CULL_MODE_FRONT;
		psoDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS;

		mMesh->fillPSO(psoDesc);
		psoDesc.VS = mVertexShader->getByteCode();
		psoDesc.PS = mPixelShader->getByteCode();

		DC(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&mPipelineState)));
	}

	void pointLight::update()
	{
		auto plcb = constBuff::create<pointLightConstants>();
		auto scb = constBuff::create<shadowConstants>();
		mPointLightConstantsAddr = plcb.gpuP;
		mShadowConstantsAddr = scb.gpuP;
		auto & plc = *plcb.cpuP;
		auto & sc = *scb.cpuP;

		Matrix4d scale;
		scale.setZero();
		double radius = mColor.maxCoeff();
		const double minimumIntensityReciprocal = 255;
		radius = sqrt(minimumIntensityReciprocal * radius); // smallest value in sRGB
		scale.diagonal() << radius, radius, radius, 1;
		scale.col(3) << mOwner->position(), 1;
		Matrix4d modelViewProjection = camera::main()->viewProjectionMatrix() * scale;
		plc.modelViewProjection = modelViewProjection.cast<float>();
		Vector4d pos;
		pos << mOwner->position(), 1;
		plc.center = (camera::main()->viewMatrix() * pos).cast<float>();
		plc.color = mColor;
															// +x -x +y -y +z -z
		Vector4d xDir(1, 0, 0, 0), yDir(0, 1, 0, 0), zDir(0, 0, 1, 0), lightPos;
		Matrix4d camViewInv = camera::main()->viewMatrix().inverse();
		xDir = camViewInv * xDir;
		yDir = camViewInv * yDir;
		zDir = camViewInv * zDir;
		lightPos = mOwner->transform().col(3);

		const double nearPlane = 0.0001;
		Matrix4d projection = perspective(M_PI / 2, 1.0, nearPlane, radius);

		{
			Matrix4d view = rightUpForwardPos<double>(-zDir, yDir, xDir, lightPos);
			sc.viewProjection[0] = (projection * view).cast<float>();
		}
		{
			Matrix4d view = rightUpForwardPos<double>(zDir, yDir, -xDir, lightPos);
			sc.viewProjection[1] = (projection * view).cast<float>();
		}
		{
			Matrix4d view = rightUpForwardPos<double>(xDir, -zDir, yDir, lightPos);
			sc.viewProjection[2] = (projection * view).cast<float>();
		}
		{
			Matrix4d view = rightUpForwardPos<double>(xDir, zDir, -yDir, lightPos);
			sc.viewProjection[3] = (projection * view).cast<float>();
		}
		{
			Matrix4d view = rightUpForwardPos<double>(xDir, yDir, zDir, lightPos);
			sc.viewProjection[4] = (projection * view).cast<float>();
		}
		{
			Matrix4d view = rightUpForwardPos<double>(-xDir, yDir, -zDir, lightPos);
			sc.viewProjection[5] = (projection * view).cast<float>();
		}
		
		sc.lightPos = lightPos.cast<float>();

		sc.nearPlane = (float) nearPlane;
		sc.farPlane = (float) radius;
		sc.nearMulFar = (float) (nearPlane * radius);
		sc.farMinNear = (float) (radius - nearPlane);

		sc.radiusSq = (float)(radius * radius);
		sc.radiusSqRecip = (float)(1.0 / (radius * radius));
		sc.resolution = config::shadowMapResolution();
		sc.resolutionRecip = 1.0f / config::shadowMapResolution();
	}
	
	pointLight * pointLight::copyTo(gameObject * target) const
	{
		// TODO
		throw "not implemented";
	}

	
	void pointLight::illuminate(ID3D12GraphicsCommandList * list)
	{
		ID3D12DescriptorHeap * heap[] = { mTextureViewHeap };
		list->SetDescriptorHeaps(1, heap);
		list->SetGraphicsRootDescriptorTable(geometryRootSig::perObjectTexturesLocation, mTextureViewHeap.getGPUHandle());
		list->SetGraphicsRootConstantBufferView(geometryRootSig::perObjectCBLocation, mPointLightConstantsAddr);
		list->SetGraphicsRootConstantBufferView(geometryRootSig::shadowCBLocation, mShadowConstantsAddr);
		list->SetPipelineState(mPipelineState.Get());
		mMesh->draw(list);
	}

	void pointLight::setGBufferTextures(renderTarget & rt)
	{
		mShadowMap->createShaderView(mTextureViewHeap.getCPUHandle());
		rt.createShaderResourceViews(mTextureViewHeap, 1);
	}

}