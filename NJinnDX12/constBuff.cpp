#include "stdafx.h"
#include "constBuff.h"

#include "dx12helper.h"
#include "context.h"

namespace nJinn{

	std::deque<constBuff::allocator> constBuff::mAllocators;

	constBuff::allocator::allocator(size_t size) : 
		size(size),
		lastBeforeEnd(false),
		full(0)
	{
		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(size),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&buffer)));

		CD3DX12_RANGE readRange(0, 0);
		DC(buffer->Map(0, &readRange, &begin));
		end = (char *)begin + size;

		current = begin;
		for (int i = 0; i < 3; ++i) {
			prev[i] = begin;
		}
		lastBeforeEnd = false;
		gpuBegin = buffer->GetGPUVirtualAddress();
		gpuCurrent = gpuBegin;

		SET_NAME(buffer, L"Constant Buffer");
	}

	constBuff::allocator::~allocator()
	{
		CD3DX12_RANGE range(0, 0);
		buffer->Unmap(0, &range);
	}

	constBuff::address<void> constBuff::allocator::allocate(size_t length)
	{
		constBuff::address<void> ret;
		char * allocEnd = (char *)current + length;
		if (!lastBeforeEnd) {
			if (allocEnd < end) {
				ret.cpuP = current;
				ret.gpuP = gpuCurrent;

				current = allocEnd;
				gpuCurrent += length;
			}
			else {
				current = begin;
				gpuCurrent = gpuBegin;
				lastBeforeEnd = true;
				ret = allocate(length);
			}
		}
		else {
			if (allocEnd < prev[0]) {
				ret.cpuP = current;
				ret.gpuP = gpuCurrent;

				current = allocEnd;
				gpuCurrent += length;
			}
			else {
				full = 1;
				ret.cpuP = nullptr;
			}
		}
		return ret;
	}

	bool constBuff::allocator::makeFree()
	{
		for (int i = 0; i < 2; ++i) {
			prev[i] = prev[i + 1];
		}
		prev[2] = current;
		lastBeforeEnd = (current < prev[0]);
		full += full;
		return (16 == full);
	}

	void constBuff::initialize()
	{
		mAllocators.emplace_back(256 * 128);
	}

	void constBuff::finalize()
	{
		mAllocators.clear();
	}

	void constBuff::free()
	{
		for (auto it = mAllocators.begin(); it != mAllocators.end(); ++it) {
			if (it->makeFree() && mAllocators.size() > 0) {
				mAllocators.pop_front();
				it = mAllocators.begin();
			}
		}
	}
}
