#include "stdafx.h"
#include "renderTexture.h"

#include "context.h"
#include "dx12helper.h"


namespace nJinn {
	renderTexture::renderTexture(uint32_t width, uint32_t height, DXGI_FORMAT format, float clearValue[4], uint32_t count) :
		mFormat(format)
	{
		mWidth = width;
		mHeight = height;
		CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Tex2D(format, width, height, count, 1);
		desc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
		D3D12_CLEAR_VALUE clearVal;
		if (clearValue) {
			memcpy(clearVal.Color, clearValue, 16);
		}
		else {
			for (int i = 0; i < 4; ++i) clearVal.Color[i] = 0;
		}
		clearVal.Format = format;

		memcpy(mClearValue, clearVal.Color, 16);

		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&desc,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
			&clearVal,
			IID_PPV_ARGS(&mResource)));

		mView = {};
		mView.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		mView.Format = format;

		mTargetViewDescriptor = {};
		mTargetViewDescriptor.Format = format;
		if (count != 1) {
			mTargetViewDescriptor.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2DARRAY;
			mTargetViewDescriptor.Texture2DArray.MipSlice = 0;
			mTargetViewDescriptor.Texture2DArray.FirstArraySlice = 0;
			mTargetViewDescriptor.Texture2DArray.ArraySize = count;
			mView.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
			mView.Texture2DArray.MipLevels = 1;
			mView.Texture2DArray.ArraySize = count;
			mView.Texture2DArray.FirstArraySlice = 0;
			mView.Texture2DArray.MostDetailedMip = 0;
		}
		else {
			mTargetViewDescriptor.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
			mTargetViewDescriptor.Texture2D.MipSlice = 0;
			mView.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
			mView.Texture2D.MipLevels = 1;
		}
	}

	renderTexture::renderTexture(uint32_t width, DXGI_FORMAT format, float clearValue[4]) :
		renderTexture(width, width, format, clearValue, 6)
	{
		mView.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
		mView.TextureCube.MipLevels = 1;
		mView.TextureCube.MostDetailedMip = 0;
		mView.TextureCube.ResourceMinLODClamp = 0;
	}

	void renderTexture::createRenderTargetViewDescriptor(D3D12_CPU_DESCRIPTOR_HANDLE handle)
	{

		device->CreateRenderTargetView(mResource.Get(), &mTargetViewDescriptor, handle);
	}
}