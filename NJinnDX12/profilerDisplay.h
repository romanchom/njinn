#pragma once

#include <d3d12.h>
#include <d3d11on12.h>
#include <d2d1_3.h>
#include <dwrite.h>
#include "config.h"
#include <map>
#include <string>
#include <fstream>

namespace nJinn {
	class profilerDisplay
	{
	public:
		struct profilerEntry {
			double cpuTime;
			double gpuTime;
		};
		profilerDisplay();
		void initialize();
		void draw();
		void flush();
		void drawString(const D2D1_RECT_F & rect, const std::wstring & text);

		std::map<std::wstring, profilerEntry> entries;
	private:
		Microsoft::WRL::ComPtr<ID3D11Device> mD3D11Dev;
		Microsoft::WRL::ComPtr<ID3D11DeviceContext> mD3D11DeviceContext;
		Microsoft::WRL::ComPtr<ID3D11On12Device> mD3D11On12Device;
		Microsoft::WRL::ComPtr<ID2D1Factory3> mD2DFactory;
		Microsoft::WRL::ComPtr<ID2D1Device2> mD2DDevice;
		Microsoft::WRL::ComPtr<ID2D1DeviceContext2> mD2DContext;
		Microsoft::WRL::ComPtr<IDWriteFactory> mDWriteFactory;
		Microsoft::WRL::ComPtr<ID3D11Resource> mWrappedBackBuffer[config::backBufferCount];
		Microsoft::WRL::ComPtr<ID2D1Bitmap1> mRenderTargets[config::backBufferCount];


		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> mTextBrush;
		Microsoft::WRL::ComPtr<IDWriteTextFormat> mTextFormat;
		std::fstream mCsv;
	};
}
