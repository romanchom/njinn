#ifndef SHADOWCONSTANTS_H
#define SHADOWCONSTANTS_H

#include "hlslTypeDefs.h"

struct shadowConstants {
	float4x4 viewProjection[6];

	float4 lightPos;

	float nearPlane;
	float farPlane;
	float nearMulFar;
	float farMinNear;

	float radiusSq;
	float radiusSqRecip;
	float resolution;
	float resolutionRecip;
};

#ifndef _WIN32
ConstantBuffer<shadowConstants> shadow : register(b2);
#endif

#endif