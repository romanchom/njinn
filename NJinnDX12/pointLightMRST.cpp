#include "stdafx.h"
#include "pointLightMRST.h"
#include "context.h"
#include "geometryRootSig.h"

#include <iostream>

namespace nJinn {
	pointLightMRST::shadowTask::shadowTask(pointLightMRST * light) : mLight(light)
	{}

	void pointLightMRST::shadowTask::execute() {
		mLight->doGenerateShadowMap(mCommandList);
		mCommandList.close();
	}

	void pointLightMRST::doGenerateShadowMap(ID3D12GraphicsCommandList * list)
	{
		mStopwatch.reset(list);
		mStopwatch.measure(0, list);

		list->SetGraphicsRootSignature(ctx->deferredRenderer.mRootSignature.Get());
		list->SetGraphicsRootConstantBufferView(geometryRootSig::shadowCBLocation, mShadowConstantsAddr);
		for (int i = 0; i < 6; ++i) {
			list->SetGraphicsRoot32BitConstant(geometryRootSig::index0, i, 0);
			renderTarget & rt = mRenderTarget[i];
			rt.set(list);
			rt.clear(list);
			drawShadowCasters(list);
			rt.endFrame(list);
		}
		
		mStopwatch.measure(1, list);
	}

	void pointLightMRST::update()
	{
		pointLight::update();
		auto & profData = ctx->profiler.entries[L"Light MR ST"];
		profData.gpuTime = mStopwatch.getDifference(0, 1) * 1000;
		profData.cpuTime = mStopwatch.getCPUDifference(0, 1) * 1000;
	}

	pointLightMRST::pointLightMRST(gameObject * owner) : 
		pointLightMR(owner),
		mShadowTask(this),
		mStopwatch(2)
	{}

	void pointLightMRST::generateShadowMap()
	{
		ctx->deferredRenderer.submitRenderingTask(&mShadowTask);
	}
}