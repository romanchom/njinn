#pragma once

#include <d3d12.h>
#include <memory>
#include "trackedResource.h"

namespace nJinn {
	typedef std::shared_ptr<class mesh> pMesh;

	class mesh : public trackedResource<mesh>
	{
	private:
		Microsoft::WRL::ComPtr<ID3D12Resource> mVertexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource> mIndexBuffer;
		D3D12_VERTEX_BUFFER_VIEW mVertexBufferView;
		D3D12_INDEX_BUFFER_VIEW mIndexBufferView;

		D3D12_INPUT_ELEMENT_DESC mAttributes[10];
		unsigned int mAttributeCount;
		unsigned int mIndexCount;

	public:
		mesh() {};
		mesh(const std::wstring & fileName);

		D3D12_INPUT_LAYOUT_DESC layoutDescription();

		void draw(ID3D12GraphicsCommandList * commandList, size_t indexCount = 1);
		void fillPSO(D3D12_GRAPHICS_PIPELINE_STATE_DESC & descriptor) const;

		static pMesh plane();
	private:

		static void initialize();
		static void finalize();

		static pMesh sPlane;
		
		void loadSbm(const std::wstring & fileName);
		void makePlane();

		friend class context;
	};
}