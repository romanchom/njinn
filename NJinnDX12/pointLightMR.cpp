#include "stdafx.h"
#include "pointLightMR.h"

#include <iostream>

namespace nJinn {
	pointLightMR::pointLightMR(gameObject * owner) : pointLight(owner),
		mRenderTarget{0, 0, 0, 0, 0, 0}
	{
		for (int i = 0; i < 6; ++i) {
			mRenderTarget[i].depthStencil(mShadowMap, i);
			mRenderTarget[i].create();
		}
	}
}