#include "objectConstants.h"

struct input {
	float3 pos : POSITION;
	float2 uv : TEX_COORD0;
	float3 normal : NORMAL0;
	float3 tangent : TANGENT0;
};

struct output {
	float3 normal : NORMAL;
	float2 uv : TEX_COORD0;
	float4 pos : SV_POSITION;
};

output main(in input i)
{
	output o;
	float4 p = float4(i.pos, 1);
	o.pos = mul(object.modelViewProjection, p);
	o.normal = mul(object.invTModelView, float4(i.normal, 0)).xyz;
	o.uv = i.uv;

	return o;
}