#pragma once
#include "texture.h"
#include <dxgi1_4.h>

namespace nJinn {
	class depthStencilTexture : public texture
	{
	private:
		D3D12_DEPTH_STENCIL_VIEW_DESC targetViewDescriptor;
		D3D12_CLEAR_FLAGS mClearFlags;
		float mDepthClearValue;
		uint8_t mStencilClearValue;
	public:
		depthStencilTexture(uint32_t width, uint32_t height, DXGI_FORMAT format, float depthClearValue = 1, uint8_t stencilClearValue = 0, uint32_t count = 1);
		depthStencilTexture(uint32_t width, DXGI_FORMAT format, float depthClearValue = 1, uint8_t stencilClearValue = 0);
		void setAsDepthStencilTarget(struct ID3D12GraphicsCommandList * commandList);
		void setAsShaderResource(struct ID3D12GraphicsCommandList * commandList);
		void createDepthStencilView(D3D12_CPU_DESCRIPTOR_HANDLE handle);
		void createDepthStencilView(D3D12_CPU_DESCRIPTOR_HANDLE handle, size_t index);
		D3D12_CLEAR_FLAGS clearFlags() { return mClearFlags; }
		float depthClearValue() { return mDepthClearValue; }
		uint8_t stencilClearValue() { return mStencilClearValue; }
	};

	typedef std::shared_ptr<depthStencilTexture> pDepthStencilTexture;
}
