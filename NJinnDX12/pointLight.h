#pragma once

#include <Eigen/Dense>
#include "lightSource.h"
#include "shader.h"
#include "depthStencilTexture.h"
#include "mesh.h"
#include "descriptorHeap.h"

namespace nJinn {
	class pointLight : public lightSource
	{
	protected:
		virtual void initialize() override;
		virtual void update() override;
		virtual void illuminate(ID3D12GraphicsCommandList * list) override;
		virtual void setGBufferTextures(class renderTarget & rt) override;

		pShader mVertexShader;
		pShader mPixelShader;
		Microsoft::WRL::ComPtr<ID3D12PipelineState> mPipelineState;
		
		pDepthStencilTexture mShadowMap;
		descriptorHeap mTextureViewHeap;

		pMesh mMesh;

		uint64_t mPointLightConstantsAddr;
		uint64_t mShadowConstantsAddr;

		Eigen::Vector4f mColor;
	public:
		virtual pointLight * copyTo(gameObject * target) const override;
		pointLight(gameObject * owner);
		void color(const Eigen::Vector3f & color) { mColor.topLeftCorner(3, 1) = color; }
		const Eigen::Vector3f & color() { return mColor.topLeftCorner(3, 1); }
	};
}
