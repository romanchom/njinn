struct input {
	float3 pos : POSITION;
};

struct output {
	float2 uv : TEX_COORD0;
	float4 pos : SV_POSITION;
};

output main(input i)
{
	output o;
	o.pos = float4(i.pos, 1);
	o.uv = i.pos.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
	return o;
}