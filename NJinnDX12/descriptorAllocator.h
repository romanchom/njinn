#pragma once

#include <d3d12.h>
#include "descriptorHeap.h"

namespace nJinn {
	struct descriptorTable {
		D3D12_GPU_DESCRIPTOR_HANDLE GPUHandle;
		D3D12_CPU_DESCRIPTOR_HANDLE CPUHandle;
		size_t size;
	};

	class descriptorAllocator
	{
	private:
		descriptorHeap mHeap;
		size_t mSlotsTaken;
	public:
		descriptorAllocator(D3D12_DESCRIPTOR_HEAP_TYPE type, size_t initialSize = 64);
		descriptorTable allocate(size_t slots);
		ID3D12DescriptorHeap * getDescriptorHeap() { return mHeap.get(); }
		friend class nJinnDevice;
	};
}

