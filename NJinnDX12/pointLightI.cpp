#include "stdafx.h"
#include "pointLightI.h"
#include "context.h"
#include "geometryRootSig.h"

namespace nJinn {
	pointLightI::shadowTask::shadowTask(pointLightI * light) :
		mLight(light)
	{}

	void pointLightI::shadowTask::execute()
	{
		mLight->doGenerateShadowMap(mCommandList);
		mCommandList.close();
	}

	void pointLightI::doGenerateShadowMap(ID3D12GraphicsCommandList * list)
	{
		mStopwatch.reset(list);
		mStopwatch.measure(0, list);

		list->SetGraphicsRootSignature(ctx->deferredRenderer.mRootSignature.Get());
		list->SetGraphicsRootConstantBufferView(geometryRootSig::shadowCBLocation, mShadowConstantsAddr);
		mInstanceDataView.BufferLocation = mShadowConstantsAddr;
		list->IASetVertexBuffers(1, 1, &mInstanceDataView);
		mRenderTarget.set(list);
		mRenderTarget.clear(list);
		drawShadowCastersInstanced(list, 6);
		mRenderTarget.endFrame(list);

		mStopwatch.measure(1, list);
	}

	void pointLightI::update()
	{
		pointLight::update();
		auto & profData = ctx->profiler.entries[L"Light I"];
		profData.gpuTime = mStopwatch.getDifference(0, 1) * 1000;
		profData.cpuTime = mStopwatch.getCPUDifference(0, 1) * 1000;
	}

	pointLightI::pointLightI(gameObject * owner) :
		pointLight(owner),
		mRenderTarget(mShadowMap),
		mShadowTask(this),
		mStopwatch(2)
	{
		mInstanceDataView.StrideInBytes = 64;
		mInstanceDataView.SizeInBytes = 64 * 6;
	}

	void pointLightI::generateShadowMap()
	{
		ctx->deferredRenderer.submitRenderingTask(&mShadowTask);
	}
}