#pragma once

#include "config.h"

#include <d3d12.h>
#include <dxgi1_4.h>
#include <Eigen/Dense>

#include "frameResources.h"

namespace nJinn {
	class window
	{
	public:
		window(uint32_t width, uint32_t height);
		Eigen::Vector2d dimentions() { return Eigen::Vector2d(mWidth, mHeight); }
		int run();
		void title(const std::wstring & name);
	private:
		unsigned int mWidth;
		unsigned int mHeight;
		std::wstring mName;
		HWND mWindowHandle;

		friend class context;

		static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	};
}
