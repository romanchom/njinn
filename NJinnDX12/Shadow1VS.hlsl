struct input {
	float3 pos : POSITION;
};

struct output {
	float4 pos : SV_POSITION;
};

#include "objectConstants.h"

output main(in input i)
{
	output o;
	o.pos = mul(object.model, float4(i.pos, 1));
	return o;
}