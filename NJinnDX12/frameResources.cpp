#include "stdafx.h"

#include "frameResources.h"
#include "dx12helper.h"
#include "context.h"

#include "stopwatch.h"

using namespace Microsoft::WRL;

namespace nJinn {
	frameResources::frameResources(ID3D12CommandQueue * queue, HWND handle, unsigned int width, unsigned int height) :
		mRenderTargetViewAllocator(D3D12_DESCRIPTOR_HEAP_TYPE_RTV, 8),
		mDepthStencilViewAllocator(D3D12_DESCRIPTOR_HEAP_TYPE_DSV, 1),
		mViewPort(),
		mScissorRect(),
		mFence(queue)
	{
		mViewPort.Width = (float) width;
		mViewPort.Height = (float) height;
		mViewPort.MaxDepth = 1;
		mViewPort.MinDepth = 0;

		mScissorRect.right = (int) width;
		mScissorRect.bottom = (int) height;

		// create swap chain
		DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
		swapChainDesc.BufferCount = config::backBufferCount;
		swapChainDesc.BufferDesc.Width = width;
		swapChainDesc.BufferDesc.Height = height;
		swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.OutputWindow = handle;
		swapChainDesc.Windowed = true;

		ComPtr<IDXGIFactory4> factory;
		DC(CreateDXGIFactory1(IID_PPV_ARGS(&factory)));

		ComPtr<IDXGISwapChain> swapChain;	
		DC(factory->CreateSwapChain(
			queue,		// Swap chain needs the queue so that it can force a flush on it.
			&swapChainDesc,
			&swapChain
			));

		DC(swapChain.As(&mSwapChain));

		// This sample does not support fullscreen transitions.
		DC(factory->MakeWindowAssociation(handle, DXGI_MWA_NO_ALT_ENTER));

		D3D12_RENDER_TARGET_VIEW_DESC rtvDesc;
		rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;
		rtvDesc.Texture2D.PlaneSlice = 0;
		rtvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;

		// initialize each resource pack
		for (unsigned int i = 0; i < config::backBufferCount; ++i) {
			resourcePack & fr = mResources[i];

			DC(mSwapChain->GetBuffer(i, IID_PPV_ARGS(fr.colorBuffer.GetAddressOf())));
			// TODO fix this shit
			fr.rtvHandle = mRenderTargetViewAllocator.allocate(1).CPUHandle; 
			device->CreateRenderTargetView(fr.colorBuffer.Get(), &rtvDesc, fr.rtvHandle);

			SET_NAME(fr.colorBuffer, L"Color framebuffer");
		}


		// create depth stencil and view
		const DXGI_FORMAT depthStenciFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;

		D3D12_CLEAR_VALUE clearValue;
		clearValue.Format = depthStenciFormat;
		clearValue.DepthStencil.Depth = 1.0f;
		clearValue.DepthStencil.Stencil = 0;

		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Tex2D(depthStenciFormat, width, height, 1, 0, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL),
			D3D12_RESOURCE_STATE_DEPTH_WRITE,
			&clearValue,
			IID_PPV_ARGS(&depthStencilBuffer)));

		D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
		dsvDesc.Format = depthStenciFormat;
		dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Flags = D3D12_DSV_FLAG_NONE;

		dsvHandle = mDepthStencilViewAllocator.allocate(1).CPUHandle;
		device->CreateDepthStencilView(depthStencilBuffer.Get(), &dsvDesc, dsvHandle);

		SET_NAME(depthStencilBuffer, L"Depth-stencil buffer");

		DC(mSwapChain->Present(1, 0));
		updateFrameIndex();

		memset(mFenceVals, 0, sizeof(mFenceVals));
		mFenceVals[frameIndex()]++;

		synchronize();
	}

	frameResources::~frameResources()
	{
		synchronize();
	}

	void frameResources::updateFrameIndex()
	{
		mFrameIndex = mSwapChain->GetCurrentBackBufferIndex();
		mCurrent = mResources + mFrameIndex;
	}


	void frameResources::present()
	{
		DC(mSwapChain->Present(1, 0));

		const uint64_t currentFenceValue = mFenceVals[frameIndex()];
		mFence.signal(currentFenceValue);

		updateFrameIndex();

		mFence.wait(mFenceVals[frameIndex()]);

		mFenceVals[frameIndex()] = currentFenceValue + 1;
	}

	void frameResources::synchronize()
	{
		mFence.signal(mFenceVals[frameIndex()]);
		mFence.wait(mFenceVals[frameIndex()]);
		mFenceVals[frameIndex()]++;
	}

	void frameResources::setAsRenderTarget(ID3D12GraphicsCommandList * commandList){
		commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mCurrent->colorBuffer.Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));
		commandList->OMSetRenderTargets(1, &(mCurrent->rtvHandle), FALSE, &dsvHandle);
		commandList->RSSetViewports(1, &mViewPort);
		commandList->RSSetScissorRects(1, &mScissorRect);
	}

	void frameResources::endFrame(ID3D12GraphicsCommandList * commandList)
	{
		commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mCurrent->colorBuffer.Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
	}

	void frameResources::clear(ID3D12GraphicsCommandList * commandList, float color[4]){
		commandList->ClearRenderTargetView(mCurrent->rtvHandle, color, 0, nullptr);
		commandList->ClearDepthStencilView(dsvHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);
	}
}