#include "stdafx.h"
#include "resourceAllocator.h"

#include "context.h"
#include "dx12helper.h"
#include "d3dx12.h"

namespace nJinn {
	using namespace Microsoft::WRL;
	ComPtr<ID3D12CommandAllocator> resourceAllocator::mCommandAllocators[] = {};
	ComPtr<ID3D12GraphicsCommandList> resourceAllocator::mCommandList;
	std::deque<resourceAllocator::uploadTask> resourceAllocator::mPendingTasks;
	unsigned int resourceAllocator::mTasksProcessed;
	unsigned int resourceAllocator::mTasksProcessedPreviously;

	void resourceAllocator::initialize()
	{
		for (int i = 0; i < config::backBufferCount; ++i) {
			DC(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&mCommandAllocators[i])));
		}
		DC(device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, mCommandAllocators[0].Get(), nullptr, IID_PPV_ARGS(&mCommandList)));
		mCommandList->Close();
		mTasksProcessed = 0;
		mTasksProcessedPreviously = 0;
	}

	void resourceAllocator::release()
	{
		mPendingTasks.clear();
		mCommandAllocators[ctx->frameIndex()].Reset();
		mCommandList.Reset();
	}

	void resourceAllocator::allocate(ComPtr<ID3D12Resource>& ret, D3D12_RESOURCE_STATES state, size_t sizeInBytes, void * data)
	{
		ComPtr<ID3D12Resource> resource;

		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes),
			D3D12_RESOURCE_STATE_GENERIC_READ, // state
			nullptr,
			IID_PPV_ARGS(&resource)));

		SET_NAME(resource, L"Temporary upload resource");

		void * dst;
		DC(resource->Map(0, nullptr, &dst));
		memcpy(dst, data, sizeInBytes);
		resource->Unmap(0, nullptr);

		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(sizeInBytes),
			state,
			nullptr,
			IID_PPV_ARGS(&ret)));

		SET_NAME(ret, L"Resource allocator resource");

		mPendingTasks.push_back({ resource, ret });
	}

	void resourceAllocator::allocate(Microsoft::WRL::ComPtr<ID3D12Resource>& ret, const D3D12_RESOURCE_DESC & desc, D3D12_RESOURCE_STATES state, Microsoft::WRL::ComPtr<ID3D12Resource>& resource)
	{
		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&desc,
			D3D12_RESOURCE_STATE_GENERIC_READ, // state
			nullptr,
			IID_PPV_ARGS(&resource)));

		SET_NAME(resource, L"Temporary upload resource");

		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&desc,
			state,
			nullptr,
			IID_PPV_ARGS(&ret)));

		SET_NAME(ret, L"Resource allocator resource");

		mPendingTasks.push_back({ resource, ret });
	}

	void resourceAllocator::executeUpload()
	{
		// reset command list
		DC(mCommandAllocators[ctx->frameIndex()]->Reset());
		DC(mCommandList->Reset(mCommandAllocators[ctx->frameIndex()].Get(), nullptr));

		// erase tasks which are done
		auto begin = mPendingTasks.begin();
		auto end = begin + mTasksProcessedPreviously;
		mPendingTasks.erase(begin, end);

		// process newly added tasks
		mTasksProcessedPreviously = mTasksProcessed;
		mTasksProcessed = 0;
		begin = mPendingTasks.begin() + mTasksProcessedPreviously;
		end = mPendingTasks.end();
		for (auto it = begin; it != end; ++it) {
			auto& task = *it;
			mCommandList->CopyResource(task.dst.Get(), task.src.Get());
			++mTasksProcessed;
		}

		// close command list
		DC(mCommandList->Close());
		ID3D12CommandList * list[] = { mCommandList.Get() };
		ctx->mainCommandQueue->ExecuteCommandLists(1, list);
	}

	void resourceAllocator::reset()
	{
		//mPendingTasks.clear();
	}
}