#pragma once

#include <unordered_set>
#include <d3d12.h>
#include "component.h"

namespace nJinn {
	class lightSource : public component
	{
	public:
		lightSource(class gameObject * owner);
		~lightSource();
		virtual lightSource * copyTo(gameObject * target) const override = 0;
	protected:
		virtual void initialize() override;
		virtual void illuminate(ID3D12GraphicsCommandList * list) = 0;
		virtual void generateShadowMap() = 0;
		virtual void setGBufferTextures(class renderTarget & rt) = 0;

		static void drawShadowCasters(ID3D12GraphicsCommandList * list);
		static void drawShadowCastersGS(ID3D12GraphicsCommandList * list);
		static void drawShadowCastersInstanced(ID3D12GraphicsCommandList * list, size_t instanceCount);
	private:
		static std::unordered_set<lightSource *> sLights;
		static void drawAllShadowMaps();
		static void drawAll(ID3D12GraphicsCommandList * list);

		friend class deferredRenderer;
	};
}
