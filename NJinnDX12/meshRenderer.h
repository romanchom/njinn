#pragma once

#include "objectConstants.h"
#include "renderer.h"
#include "descriptorHeap.h"
#include "constBuff.h"
#include "commandBundle.h"
#include "shader.h"
#include "mesh.h"
#include "texture.h"

namespace nJinn {
	class meshRenderer : public renderer
	{
	public:
		meshRenderer(class gameObject * owner);
		~meshRenderer();
		

		pMesh mMesh;
		pTexture mTexture;

		virtual meshRenderer * copyTo(class gameObject * target) const override;
	protected:
		virtual void draw(ID3D12GraphicsCommandList * list) override;
		virtual void drawDepthOnly(ID3D12GraphicsCommandList * list) override;
		virtual void drawDepthOnlyGS(ID3D12GraphicsCommandList * list) override;
		virtual void drawDepthOnlyInstanced(ID3D12GraphicsCommandList * list, size_t instanceCount) override;
		virtual void initialize() override;
		virtual void update() override;
	private:
		descriptorHeap mTextureViewHeap;
		uint64_t mConstBuffAddr;
		commandBundle mBundle;

		pShader mVertexShader;
		pShader mPixelShader;
		Microsoft::WRL::ComPtr<ID3D12PipelineState> mPipelineState;

		pShader mShadowPixelShader;

		// simple, multipass
		pShader mVS0;
		Microsoft::WRL::ComPtr<ID3D12PipelineState> mPSO0;

		// geometry shader
		pShader mGS1;
		pShader mVS1;
		Microsoft::WRL::ComPtr<ID3D12PipelineState> mPSO1;

		// instancing
		pShader mVS2;
		Microsoft::WRL::ComPtr<ID3D12PipelineState> mPSO2;
	};
}
