#pragma once

#include <d3d12.h>
#include <dxgi1_4.h>
#include "dx12helper.h"
#include "descriptorAllocator.h"
#include "config.h"
#include "fence.h"

namespace nJinn {
	class frameResources
	{
	private:
		descriptorAllocator mRenderTargetViewAllocator;
		descriptorAllocator mDepthStencilViewAllocator;

		struct resourcePack {
			Microsoft::WRL::ComPtr<ID3D12Resource> colorBuffer;
			CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle;
		};
		resourcePack mResources[config::backBufferCount];
		resourcePack * mCurrent;

		Microsoft::WRL::ComPtr<ID3D12Resource> depthStencilBuffer;
		CD3DX12_CPU_DESCRIPTOR_HANDLE dsvHandle;

		Microsoft::WRL::ComPtr<IDXGISwapChain3> mSwapChain;
		unsigned int mFrameIndex;
		void updateFrameIndex();

		D3D12_VIEWPORT mViewPort;
		D3D12_RECT mScissorRect;

		fence mFence;
		uint64_t mFenceVals[config::backBufferCount];
	public:
		frameResources(ID3D12CommandQueue * queue, HWND handle, unsigned int width, unsigned int height);
		~frameResources();
		void present();
		void setAsRenderTarget(ID3D12GraphicsCommandList * commandList);
		void endFrame(ID3D12GraphicsCommandList * commandList);
		void clear(ID3D12GraphicsCommandList * commandList, float color[4]);
		void synchronize();
		resourcePack * operator->() { return mCurrent; }
		unsigned int frameIndex() const { return mFrameIndex; }
		resourcePack * operator[](size_t index) { return mResources + index; }
	};
}