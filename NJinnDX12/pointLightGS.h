#pragma once

#include "pointLight.h"
#include "renderTarget.h"
#include "gpuStopwatch.h"
#include "renderingTask.h"

namespace nJinn {
	class pointLightGS : public pointLight
	{
	private:
		class shadowTask : public renderingTask {
		public:
			shadowTask(pointLightGS * light);
			virtual void execute() override;
		private:
			pointLightGS * mLight;
		};

		void doGenerateShadowMap(ID3D12GraphicsCommandList * list);

		renderTarget mRenderTarget;
		shadowTask mShadowTask;

		gpuStopwatch mStopwatch;
	protected:
		virtual void update() override;
	public:
		pointLightGS(gameObject * owner);
		virtual void generateShadowMap() override;
	};
}
