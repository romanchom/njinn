#include "stdafx.h"
#include "descriptorHeap.h"

#include "dx12helper.h"
#include "context.h"

namespace nJinn {
	descriptorHeap::descriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE type, size_t initialSize) :
		mSize(initialSize)
	{
		D3D12_DESCRIPTOR_HEAP_DESC descriptorDesc;
		descriptorDesc.Type = type;
		if (type == D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV || 
			type == D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER) {
			descriptorDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		}
		else {
			descriptorDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		}
		
		descriptorDesc.NumDescriptors = (UINT) (initialSize);
		descriptorDesc.NodeMask = 0;

		DC(device->CreateDescriptorHeap(&descriptorDesc, IID_PPV_ARGS(&mDescriptorHeap)));

		mIncrement = device->GetDescriptorHandleIncrementSize(type);
	}

	D3D12_CPU_DESCRIPTOR_HANDLE descriptorHeap::getCPUHandle(size_t index)
	{
		D3D12_CPU_DESCRIPTOR_HANDLE ret = mDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
		ret.ptr += mIncrement * index;
		return ret;
	}

	D3D12_GPU_DESCRIPTOR_HANDLE descriptorHeap::getGPUHandle(size_t index)
	{
		D3D12_GPU_DESCRIPTOR_HANDLE ret = mDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
		ret.ptr += mIncrement * index;
		return ret;
	}

}