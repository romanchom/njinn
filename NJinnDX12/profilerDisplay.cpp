#include "stdafx.h"
#include "profilerDisplay.h"

#include <sstream>
#include "dx12helper.h"
#include "context.h"

using namespace Microsoft::WRL;

namespace nJinn {

	const int textHeight = 20;

	profilerDisplay::profilerDisplay() : 
		mCsv("profiler.csv", std::ios::out)
	{
	}

	void profilerDisplay::initialize()
	{
		UINT d3d11DeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT | D3D11_CREATE_DEVICE_DEBUG;
		D2D1_FACTORY_OPTIONS d2dFactoryOptions = {};
		d2dFactoryOptions.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;
		ID3D12CommandQueue * ptr = ctx->mainCommandQueue.get();
		DC(D3D11On12CreateDevice(
			device,
			d3d11DeviceFlags,
			nullptr,
			0,
			reinterpret_cast<IUnknown**>(&ptr),
			1,
			0,
			&mD3D11Dev,
			&mD3D11DeviceContext,
			nullptr));


		DC(mD3D11Dev.As(&mD3D11On12Device));

		D2D1_DEVICE_CONTEXT_OPTIONS deviceOptions = D2D1_DEVICE_CONTEXT_OPTIONS_NONE;
		DC(D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, __uuidof(ID2D1Factory3), &d2dFactoryOptions, &mD2DFactory));
		ComPtr<IDXGIDevice> dxgiDevice;
		DC(mD3D11On12Device.As(&dxgiDevice));
		DC(mD2DFactory->CreateDevice(dxgiDevice.Get(), &mD2DDevice));
		DC(mD2DDevice->CreateDeviceContext(deviceOptions, &mD2DContext));
		DC(DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), &mDWriteFactory));
		
		float dpiX;
		float dpiY;
		mD2DFactory->GetDesktopDpi(&dpiX, &dpiY);
		D2D1_BITMAP_PROPERTIES1 bitmapProperties = D2D1::BitmapProperties1(
			D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
			dpiX,
			dpiY
			);

		D3D11_RESOURCE_FLAGS d3d11Flags = { D3D11_BIND_RENDER_TARGET };

		for (int i = 0; i < config::backBufferCount; ++i) {
			//DC(m_swapChain->GetBuffer(n, IID_PPV_ARGS(&m_renderTargets[n])));
			//m_d3d12Device->CreateRenderTargetView(m_renderTargets[n].Get(), nullptr, rtvHandle);
			
			DC(mD3D11On12Device->CreateWrappedResource(
				ctx->frameResources[i]->colorBuffer.Get(),
				&d3d11Flags,
				D3D12_RESOURCE_STATE_RENDER_TARGET,
				D3D12_RESOURCE_STATE_PRESENT,
				IID_PPV_ARGS(&mWrappedBackBuffer[i])
				));

			// Create a render target for D2D to draw directly to this back buffer.
			ComPtr<IDXGISurface> surface;
			DC(mWrappedBackBuffer[i].As(&surface));
			DC(mD2DContext->CreateBitmapFromDxgiSurface(
				surface.Get(),
				&bitmapProperties,
				&mRenderTargets[i]
				));

			DC(mD2DContext->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), &mTextBrush));
			DC(mDWriteFactory->CreateTextFormat(
				L"Verdana",
				NULL,
				DWRITE_FONT_WEIGHT_NORMAL,
				DWRITE_FONT_STYLE_NORMAL,
				DWRITE_FONT_STRETCH_NORMAL,
				textHeight,
				L"en-us",
				&mTextFormat
				));

			DC(mTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING));
			DC(mTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR));
		}
	}

	void profilerDisplay::draw() {
		ID2D1Bitmap1 * rt = mRenderTargets[ctx->frameIndex()].Get();
		ID3D11Resource * res = mWrappedBackBuffer[ctx->frameIndex()].Get();

		D2D1_SIZE_F rtSize = rt->GetSize();
		D2D1_RECT_F textRect = D2D1::RectF(0, 0, rtSize.width, rtSize.height);

		// Acquire our wrapped render target resource for the current back buffer.
		mD3D11On12Device->AcquireWrappedResources(&res, 1);

		// Render text directly to the back buffer.
		mD2DContext->SetTarget(rt);
		mD2DContext->BeginDraw();

		mD2DContext->SetTransform(D2D1::Matrix3x2F::Identity());
		int i = 1;
		std::wstringstream s;
		textRect = D2D1::RectF(200, 5, 400, 5 + textHeight);
		mD2DContext->DrawTextW(
			L"CPU Time",
			8,
			mTextFormat.Get(),
			&textRect,
			mTextBrush.Get()
			);

		textRect = D2D1::RectF(400, 5, 600, 5 + textHeight);
		mD2DContext->DrawTextW(
			L"GPU Time",
			8,
			mTextFormat.Get(),
			&textRect,
			mTextBrush.Get()
			);

		for (auto pair : entries) {
			float y = i * (textHeight + 5) + 5;
			textRect = D2D1::RectF(10, y, 200, y + textHeight);
			drawString(textRect, pair.first);

			textRect = D2D1::RectF(210, y, 400, y + textHeight);
			s.str(std::wstring());
			s << pair.second.cpuTime;
			mCsv << pair.second.cpuTime << "; ";
			drawString(textRect, s.str());

			textRect = D2D1::RectF(410, y, rtSize.width, y + textHeight);
			s.str(std::wstring());
			s << pair.second.gpuTime;
			mCsv << pair.second.gpuTime << "; ";
			drawString(textRect, s.str());
			++i;
		}
		mCsv << "\n";

		D2D1_TAG error1(0), error2(0);
		DC(mD2DContext->EndDraw(&error1, &error2));

		mD3D11On12Device->ReleaseWrappedResources(&res, 1);
		mD3D11DeviceContext->Flush();
	}

	void profilerDisplay::drawString(const D2D1_RECT_F & rect, const std::wstring & text)
	{
		mD2DContext->DrawTextW(
			text.c_str(),
			text.length(),
			mTextFormat.Get(),
			&rect,
			mTextBrush.Get()
			);
	}
}