#include "stdafx.h"
#include "depthStencilTexture.h"

#include "context.h"
#include "dx12helper.h"

namespace nJinn {
	depthStencilTexture::depthStencilTexture(uint32_t width, uint32_t height, DXGI_FORMAT format, float depthClearValue, uint8_t stencilClearValue, uint32_t count) :
		mDepthClearValue(depthClearValue),
		mStencilClearValue(stencilClearValue)
	{
		mWidth = width;
		mHeight = height;

		DXGI_FORMAT internalFormat;
		DXGI_FORMAT shaderViewFormat;
		switch (format) {
		case DXGI_FORMAT_D24_UNORM_S8_UINT:
			internalFormat = DXGI_FORMAT_R24G8_TYPELESS;
			shaderViewFormat = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
			mClearFlags = D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL;
			break;
		case DXGI_FORMAT_D32_FLOAT:
			internalFormat = DXGI_FORMAT_R32_TYPELESS;
			shaderViewFormat = DXGI_FORMAT_R32_FLOAT;
			mClearFlags = D3D12_CLEAR_FLAG_DEPTH;
			break;
		case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
			internalFormat = DXGI_FORMAT_R32G8X24_TYPELESS;
			shaderViewFormat = DXGI_FORMAT_R32_FLOAT;
			mClearFlags = D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL;
			break;

		default: throw std::runtime_error("Unsupported depth stencil format");
		}

		CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Tex2D(internalFormat, width, height, count, 1);
		desc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
		D3D12_CLEAR_VALUE clearVal;
		clearVal.DepthStencil.Depth = depthClearValue;
		clearVal.DepthStencil.Stencil = stencilClearValue;
		clearVal.Format = format;
		DC(device->CreateCommittedResource(
			&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&desc,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
			&clearVal,
			IID_PPV_ARGS(&mResource)));

		mView = {};
		mView.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		mView.Format = shaderViewFormat;

		targetViewDescriptor = {};
		targetViewDescriptor.Format = format;
		if (count != 1) {
			targetViewDescriptor.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2DARRAY;
			targetViewDescriptor.Texture2DArray.MipSlice = 0;
			targetViewDescriptor.Texture2DArray.FirstArraySlice = 0;
			targetViewDescriptor.Texture2DArray.ArraySize = count;
			mView.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
			mView.Texture2DArray.MipLevels = 1;
			mView.Texture2DArray.ArraySize = count;
			mView.Texture2DArray.FirstArraySlice = 0;
			mView.Texture2DArray.MostDetailedMip = 0;
		}
		else {
			targetViewDescriptor.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
			targetViewDescriptor.Texture2D.MipSlice = 0;
			mView.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
			mView.Texture2D.MipLevels = 1;
		}
	}

	depthStencilTexture::depthStencilTexture(uint32_t width, DXGI_FORMAT format, float depthClearValue, uint8_t stencilClearValue) :
		depthStencilTexture(width, width, format, depthClearValue, stencilClearValue, 6)
	{
		mView.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
		mView.TextureCube.MipLevels = 1;
		mView.TextureCube.MostDetailedMip = 0;
		mView.TextureCube.ResourceMinLODClamp = 0;
	}

	void depthStencilTexture::setAsDepthStencilTarget(ID3D12GraphicsCommandList * commandList)
	{
		CD3DX12_RESOURCE_BARRIER barrier;
		barrier.Transition(mResource.Get(), D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_DEPTH_WRITE);
		commandList->ResourceBarrier(1, &barrier);
	}

	void depthStencilTexture::setAsShaderResource(ID3D12GraphicsCommandList * commandList)
	{
		CD3DX12_RESOURCE_BARRIER barrier;
		barrier.Transition(mResource.Get(), D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		commandList->ResourceBarrier(1, &barrier);
	}

	void depthStencilTexture::createDepthStencilView(D3D12_CPU_DESCRIPTOR_HANDLE handle)
	{
		device->CreateDepthStencilView(mResource.Get(), &targetViewDescriptor, handle);
	}

	void depthStencilTexture::createDepthStencilView(D3D12_CPU_DESCRIPTOR_HANDLE handle, size_t index)
	{
		D3D12_DEPTH_STENCIL_VIEW_DESC dsv = {};
		dsv.Format = targetViewDescriptor.Format;
		dsv.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2DARRAY;
		dsv.Texture2DArray.MipSlice = 0;
		dsv.Texture2DArray.FirstArraySlice = index;
		dsv.Texture2DArray.ArraySize = 1;
		device->CreateDepthStencilView(mResource.Get(), &dsv, handle);
	}
}