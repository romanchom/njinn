#pragma once

#include <d3d12.h>
#include "dx12helper.h"

namespace nJinn {
	class fence
	{
	private:
		ID3D12CommandQueue * mQueue;
		Microsoft::WRL::ComPtr<ID3D12Fence> mFence;
		HANDLE mEvent;
		uint64_t mValue;
	public:
		fence();
		fence(ID3D12CommandQueue * queue);
		~fence();

		void signal(uint64_t value);
		void wait(uint64_t value);
	};
}
