#pragma once

#include <unordered_set>
#include <d3d12.h>

#include "component.h"

namespace nJinn {

	class renderer : public component
	{
	public:
		renderer(class gameObject * owner);
		~renderer();

	protected:
		virtual void draw(ID3D12GraphicsCommandList * list) {};
		virtual void drawDepthOnly(ID3D12GraphicsCommandList * list) {};
		virtual void drawDepthOnlyGS(ID3D12GraphicsCommandList * list) {};
		virtual void drawDepthOnlyInstanced(ID3D12GraphicsCommandList * list, size_t instanceCount) {};
	private:
		static std::unordered_set<class renderer*> sRenderers;
		static void drawAll(ID3D12GraphicsCommandList * list);
		static void drawDepthOnlyAll(ID3D12GraphicsCommandList * list);
		static void drawDepthOnlyGSAll(ID3D12GraphicsCommandList * list);
		static void drawDepthOnlyInstancedAll(ID3D12GraphicsCommandList * list, size_t instanceCount);

		friend class deferredRenderer;
		friend class lightSource;
	};


}