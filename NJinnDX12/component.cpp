#include "stdafx.h"
#include "component.h"
#include "context.h"

namespace nJinn {
	std::vector<component *> component::sUninitializedComponents;
	component::sortedComponents_t component::sSortedComponents;

	component::component(gameObject * owner, int updatePriority) : mOwner(owner)
	{
		sUninitializedComponents.push_back(this);
		mSortedComponentsLocation = sSortedComponents.insert(std::make_pair(updatePriority, this));
	}

	component::~component()
	{
		sSortedComponents.erase(mSortedComponentsLocation);
	}

	void component::initializeNew()
	{
		for (component * c : sUninitializedComponents) {
			c->initialize();
		}
		sUninitializedComponents.clear();
	}

	void component::updateComponents()
	{
		for (auto c : sSortedComponents) {
			c.second->update();
		}
	}
}