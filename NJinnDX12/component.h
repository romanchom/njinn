#pragma once

#include <vector>
#include <map>

namespace nJinn {
	class component
	{
	public:
		component(class gameObject * owner, int updatePriority);
		virtual ~component();

		virtual component * copyTo(class gameObject * target) const = 0;
	protected:
		class gameObject * mOwner;
		virtual void initialize() = 0;
		virtual void update() = 0;
	private:
		static std::vector<component *> sUninitializedComponents;
		static void initializeNew();


		friend class gameObject;
		friend class application;

	public:
		typedef std::multimap<int, component *> sortedComponents_t;
	private:
		sortedComponents_t::iterator mSortedComponentsLocation;
		static sortedComponents_t sSortedComponents;
		static void updateComponents();
	};
}
