#pragma once

#include "pointLightMR.h"
#include "renderTarget.h"
#include "gpuStopwatch.h"
#include "renderingTask.h"

namespace nJinn {
	class pointLightMRST : public pointLightMR
	{
	private:
		class shadowTask : public renderingTask {
		public:
			shadowTask(pointLightMRST * light);
			virtual void execute() override;
		private:
			pointLightMRST * mLight;
		};

		void doGenerateShadowMap(ID3D12GraphicsCommandList * list);

		shadowTask mShadowTask;
	protected:
		virtual void update() override;
		virtual void generateShadowMap() override;

		gpuStopwatch mStopwatch;
	public:
		pointLightMRST(gameObject * owner);
	};
}
