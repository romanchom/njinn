#pragma once

#include <d3d12.h>
#include "descriptorHeap.h"
#include "renderTexture.h"
#include "depthStencilTexture.h"

namespace nJinn {
	class renderTarget
	{
	protected:
		uint32_t mRenderTargetCount;
		uint32_t mBarrierCount;
		pRenderTexture mRenderTextures[8];
		D3D12_RESOURCE_BARRIER mBarriers[18];
		pDepthStencilTexture mDepthStencliTexture;
		descriptorHeap mSRVHeap;
		descriptorHeap mRTVHeap;
		descriptorHeap mDSVHeap;
		D3D12_VIEWPORT mViewport;
		D3D12_RECT mScissorRect;

		D3D12_CPU_DESCRIPTOR_HANDLE mRTVHandle, mDSVHandle;
		D3D12_CPU_DESCRIPTOR_HANDLE * mpRTV, *mpDSV;
	public:
		template<typename...Args>
		renderTarget(pDepthStencilTexture depthTexture, Args...textures);
		
		renderTarget(uint32_t renderTargetCount);

		void depthStencil(pDepthStencilTexture depthTexture, size_t index = -1);

		void set(ID3D12GraphicsCommandList * commandList);
		void setAsShaderResource(ID3D12GraphicsCommandList * commandList);
		void createShaderResourceViews(descriptorHeap & heap, uint32_t index);
		void endFrame(ID3D12GraphicsCommandList * commandList);
		void clear(ID3D12GraphicsCommandList * commandList);
		void create();
	protected:
		template<typename ...Args>
		void setRenderTextures(uint32_t index, pRenderTexture texture, Args ... args);


		void setRenderTextures(uint32_t) {};

	};


	template<>
	void renderTarget::setRenderTextures<>(uint32_t index, pRenderTexture texture);

	template<typename ...Args>
	renderTarget::renderTarget(pDepthStencilTexture depthTexture, Args ...textures) : renderTarget(sizeof...(textures))
	{
		if (depthTexture) {
			depthTexture->createDepthStencilView(mDSVHeap.getCPUHandle());
			depthTexture->createShaderView(mSRVHeap.getCPUHandle(mRenderTargetCount));
			mDepthStencliTexture = depthTexture;
		}
		setRenderTextures(0, textures...);
		create();
	}

	template<typename ...Args>
	inline void renderTarget::setRenderTextures(uint32_t index, pRenderTexture texture, Args ...args)
	{
		setRenderTextures(index, texture);
		setRenderTextures(index + 1, args...);
	}
}
