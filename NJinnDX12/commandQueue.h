#pragma once

#include <d3d12.h>

namespace nJinn {
	class commandQueue
	{
	private:
		Microsoft::WRL::ComPtr<ID3D12CommandQueue> mQueue;
	public:
		commandQueue(D3D12_COMMAND_LIST_TYPE type, int priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL, D3D12_COMMAND_QUEUE_FLAGS flags = D3D12_COMMAND_QUEUE_FLAG_NONE, unsigned nodeMask = 0);

		ID3D12CommandQueue * get() { return mQueue.Get(); }
		ID3D12CommandQueue * operator->() { return get(); }
		operator ID3D12CommandQueue * () { return get(); }
	};
}