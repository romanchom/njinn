struct input {
	float3 pos : POSITION;
};

struct output {
	float4 pos : SV_POSITION;
};

#include "pointLightConstants.h"

output main(input i)
{
	output o;
	o.pos = mul(light.modelViewProjection, float4(i.pos, 1));
	return o;
}