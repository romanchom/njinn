#include "stdafx.h"
#include "renderTarget.h"

#include "context.h"

namespace nJinn {
	renderTarget::renderTarget(uint32_t renderTargetCount) :
		mRenderTargetCount(renderTargetCount),
		mSRVHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, renderTargetCount + 1),
		mRTVHeap(D3D12_DESCRIPTOR_HEAP_TYPE_RTV, renderTargetCount + 1),
		mDSVHeap(D3D12_DESCRIPTOR_HEAP_TYPE_DSV, 1),
		mpRTV(nullptr),
		mpDSV(nullptr),
		mViewport(),
		mScissorRect()
	{}

	void renderTarget::depthStencil(pDepthStencilTexture depthTexture, size_t index)
	{
		if (depthTexture) {
			if(index == -1)
				depthTexture->createDepthStencilView(mDSVHeap.getCPUHandle());
			else
				depthTexture->createDepthStencilView(mDSVHeap.getCPUHandle(), index);

			depthTexture->createShaderView(mSRVHeap.getCPUHandle(mRenderTargetCount));
		}
		mDepthStencliTexture = depthTexture;
	}


	template<>
	void renderTarget::setRenderTextures<>(uint32_t index, pRenderTexture texture) {
		texture->createRenderTargetViewDescriptor(mRTVHeap.getCPUHandle(index));
		texture->createShaderView(mSRVHeap.getCPUHandle(index));
		mRenderTextures[index] = texture;
	}

	void renderTarget::create()
	{
		uint32_t width(0);
		uint32_t height(0);
		bool dimentions(false);

		if (mDepthStencliTexture) {
			mDSVHandle = mDSVHeap.getCPUHandle();
			mpDSV = &mDSVHandle;
			width = mDepthStencliTexture->width();
			height = mDepthStencliTexture->height();
			dimentions = true;
		}
		if (mRenderTargetCount > 0) {
			mRTVHandle = mRTVHeap.getCPUHandle();
			mpRTV = &mRTVHandle;
			for (uint32_t i = 0; i < mRenderTargetCount; ++i) {
				if (dimentions) {
					if (width != mRenderTextures[i]->width() &&
						height != mRenderTextures[i]->height()) {
						throw std::runtime_error("Cannot create render target with different sized textures.");
					}
				}
				else {
					width = mRenderTextures[i]->width();
					height = mRenderTextures[i]->height();
					dimentions = true;
				}
			}
		}

		mBarrierCount = mRenderTargetCount;
		uint32_t begin = 0;
		if (mDepthStencliTexture) {
			++mBarrierCount;
			begin = 1;
			mBarriers[0] = CD3DX12_RESOURCE_BARRIER::Transition(mDepthStencliTexture->get(),
				D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_DEPTH_WRITE);

			mBarriers[mBarrierCount] = CD3DX12_RESOURCE_BARRIER::Transition(mDepthStencliTexture->get(),
				D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		}

		for (uint32_t i = 0; i < mRenderTargetCount; ++i) {
			mBarriers[begin + i] = CD3DX12_RESOURCE_BARRIER::Transition(mRenderTextures[i]->get(), 
				D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_RENDER_TARGET);

			mBarriers[begin + i + mBarrierCount] = CD3DX12_RESOURCE_BARRIER::Transition(mRenderTextures[i]->get(),
				D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
		}

		// TODO fix this
		mViewport.Height = (float) (height);
		mViewport.Width = (float) (width);
		mViewport.MaxDepth = 1;

		mScissorRect.right = width;
		mScissorRect.bottom = height;
	}

	void renderTarget::set(ID3D12GraphicsCommandList * commandList)
	{		 
		commandList->ResourceBarrier(mBarrierCount, mBarriers);
		commandList->OMSetRenderTargets(mRenderTargetCount, mpRTV, true, mpDSV);
		commandList->RSSetViewports(1, &mViewport);
		commandList->RSSetScissorRects(1, &mScissorRect);
	}

	void renderTarget::endFrame(ID3D12GraphicsCommandList * commandList)
	{
		commandList->ResourceBarrier(mBarrierCount, mBarriers + mBarrierCount);
	}

	void renderTarget::clear(ID3D12GraphicsCommandList * commandList)
	{
		if (mDepthStencliTexture) {
			commandList->ClearDepthStencilView(mDSVHandle,
				mDepthStencliTexture->clearFlags(),
				mDepthStencliTexture->depthClearValue(),
				mDepthStencliTexture->stencilClearValue(),
				0, nullptr);
		}

		for (uint32_t i = 0; i < mRenderTargetCount; ++i) {
			commandList->ClearRenderTargetView(mRTVHeap.getCPUHandle(i),
				mRenderTextures[i]->clearValue(),
				0, nullptr);
		}
	}

	void renderTarget::setAsShaderResource(ID3D12GraphicsCommandList * commandList)
	{
		ID3D12DescriptorHeap * heap = mSRVHeap.get();
		commandList->SetDescriptorHeaps(1, &heap);
	}

	void renderTarget::createShaderResourceViews(descriptorHeap & heap, uint32_t index)
	{
		for (uint32_t i = 0; i < mRenderTargetCount; ++i) {
			mRenderTextures[i]->createShaderView(heap.getCPUHandle(index + i));
		}
		mDepthStencliTexture->createShaderView(heap.getCPUHandle(index + mRenderTargetCount));
	}
}