#include "stdafx.h"
#include "pointLightMRMT.h"
#include "context.h"
#include "geometryRootSig.h"

#include <iostream>

namespace nJinn {
	void pointLightMRMT::doGenerateShadowMap(ID3D12GraphicsCommandList * list, uint32_t index)
	{
		if (index == 0) {
			mStopwatch.reset(list);
		}
		mStopwatch.measure(index * 2, list);

		list->SetGraphicsRootSignature(ctx->deferredRenderer.mRootSignature.Get());
		list->SetGraphicsRootConstantBufferView(geometryRootSig::shadowCBLocation, mShadowConstantsAddr);

		list->SetGraphicsRoot32BitConstant(geometryRootSig::index0, index, 0);
		renderTarget & rt = mRenderTarget[index];
		rt.set(list);
		rt.clear(list);
		drawShadowCasters(list);
		rt.endFrame(list);

		mStopwatch.measure(index * 2 + 1, list);
	}

	void pointLightMRMT::update()
	{
		pointLight::update();
		auto & profData = ctx->profiler.entries[L"Light MR MT"];
		profData.gpuTime = mStopwatch.getDifference(0, 11) * 1000;
		double totalCPUTime = 0;
		for (int i = 0; i < 6; ++i) {
			totalCPUTime += mStopwatch.getCPUDifference(i * 2, i * 2 + 1);
		}
		profData.cpuTime = totalCPUTime * 1000;
	}

	pointLightMRMT::pointLightMRMT(gameObject * owner) : pointLightMR(owner),
		mShadowTasks{ 
			shadowTask(this, 0),
			shadowTask(this, 1),
			shadowTask(this, 2),
			shadowTask(this, 3),
			shadowTask(this, 4),
			shadowTask(this, 5)
		},
		mStopwatch(12)
	{}

	void pointLightMRMT::generateShadowMap()
	{
		for (int i = 0; i < 6; ++i) {
			ctx->deferredRenderer.submitRenderingTask(mShadowTasks + i);
		}
	}

	pointLightMRMT::shadowTask::shadowTask(pointLightMRMT * light, uint32_t index) :
		mLight(light),
		mIndex(index)
	{}

	void pointLightMRMT::shadowTask::execute()
	{
		mLight->doGenerateShadowMap(mCommandList, mIndex);
		mCommandList.close();
	}
}