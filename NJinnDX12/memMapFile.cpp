#include "stdafx.h"
#include "memMapFile.h"

namespace win {
	memMapFile::memMapFile(const std::wstring & fileName)
	{
		mFile = CreateFile(fileName.c_str(),
			GENERIC_READ,
			0,
			0,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			0);

		if (mFile == INVALID_HANDLE_VALUE) throw std::runtime_error("Cannot open file.");

		GetFileSizeEx(mFile, (LARGE_INTEGER*) &mFileSize);

		mFileMapping = CreateFileMapping(mFile, nullptr, PAGE_READONLY, 0, 0, nullptr);

		if (mFileMapping == INVALID_HANDLE_VALUE) {
			CloseHandle(mFile);
			throw std::runtime_error("Cannot map file to memory");
		}

		mPointer = MapViewOfFile(mFileMapping, FILE_MAP_READ, 0, 0, 0);
	}

	memMapFile::~memMapFile()
	{
		UnmapViewOfFile(mPointer);
		CloseHandle(mFileMapping);
		CloseHandle(mFile);
	}
}