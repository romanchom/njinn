#pragma once

#include <d3d12.h>
#include <map>

#include "trackedResource.h"

namespace nJinn {
	typedef std::shared_ptr<class shader> pShader;

	enum shaderType {
		vertex = 0,
		pixel,
		geometry,
	};

	struct shaderDesc {
		std::wstring name;
		shaderType type;
		bool operator<(const shaderDesc & other) const {
			if (type < other.type) {
				return true;
			}
			else if (type == other.type) {
				if (name < other.name) {
					return true;
				}
			}
			return false;
		}
	};

	class shader : public trackedResource<shader, shaderDesc>
	{
	public:
	private:

		Microsoft::WRL::ComPtr<ID3DBlob> byteCode;
		D3D12_SHADER_BYTECODE byteCodeStruct;
	public:
		shader(const shaderDesc & desc);
		const D3D12_SHADER_BYTECODE & getByteCode() { return byteCodeStruct; }
		operator const D3D12_SHADER_BYTECODE & () const { return byteCodeStruct; }
	};

}