#ifndef OBJECTCONSTANTS_H
#define OBJECTCONSTANTS_H

#include "hlslTypeDefs.h"

struct objectConstants{
	float4x4 model;
	float4x4 modelView;
	float4x4 modelViewProjection;
	float4x4 invTModelView;
};

#ifndef _WIN32
ConstantBuffer<objectConstants> object : register(b1);
#endif

#endif