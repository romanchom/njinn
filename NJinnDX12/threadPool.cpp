#include "stdafx.h"
#include "threadPool.h"
#include "dx12helper.h"

#include <comdef.h>
using namespace std;

namespace nJinn {

	void threadPool::workerFunction()
	{
		while (true) {
			task * t;
			{
				unique_lock<mutex> lock(mMutex);
				while (mTaskQueue.empty() && mShouldRun) {
					++mIdleThreads;
					if((mTaskQueue.empty()) && (mIdleThreads == mWorkerCount)) mConditionVariableIdle.notify_one();
					mConditionVariable.wait(lock);
					--mIdleThreads;
				}
				if (!mShouldRun) break;

				t = mTaskQueue.front();
				mTaskQueue.pop();
			}
			t->execute();
		}
	}

	threadPool::threadPool(uint32_t workerCount) : 
		mWorkerCount(workerCount),
		mWorkers(new std::thread[workerCount]),
		mShouldRun(true),
		mIdleThreads(0)
	{
		for (int i = 0; i < workerCount; ++i) {
			mWorkers[i] = std::thread(&threadPool::workerFunction, this);
		}
	}

	threadPool::~threadPool()
	{
		mShouldRun = false;
		mConditionVariable.notify_all();
		for (unsigned i = 0; i < mWorkerCount; ++i) {
			mWorkers[i].join();
		}
		delete[] mWorkers;
	}

	void threadPool::submitTask(task * t)
	{
		unique_lock<mutex> lock(mMutex);

		mTaskQueue.push(t);
		mConditionVariable.notify_one();
	}

	void threadPool::waitUntillCompleted()
	{
		unique_lock<mutex> lock(mMutex);
		while ((!mTaskQueue.empty()) || (mIdleThreads != mWorkerCount)) {
			mConditionVariableIdle.wait(lock);
		}
	}
}