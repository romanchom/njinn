#include "shadowConstants.h"

struct GSOutput
{
	//float3 positionDiff : POS_DIFF;
	float4 pos : SV_POSITION;
	uint rtvID : SV_RenderTargetArrayIndex;
};

[maxvertexcount(3 * 6)]
void main(triangle float4 input[3] : SV_POSITION, inout TriangleStream<GSOutput> output)
{
	for (uint f = 0; f < 6; ++f) {
		GSOutput element;
		element.rtvID = f;
		for (uint i = 0; i < 3; ++i)
		{
			element.pos = mul(shadow.viewProjection[f], input[i]);
			//element.positionDiff = (input[i] - shadow.lightPos).xyz;
			output.Append(element);
		}
		output.RestartStrip();
	}
}