#pragma once
#include "pointLight.h"
#include "renderTarget.h"
#include "gpuStopwatch.h"
#include "renderingTask.h"

namespace nJinn {
	class pointLightMR : public pointLight
	{
	protected:
		renderTarget mRenderTarget[6];
	public:
		pointLightMR(gameObject * owner);
	};
}
