#pragma once

#include <unordered_set>
#include <Eigen/Dense>
#include "component.h"

namespace nJinn {
	class gameObject;
}

namespace std {
	template<>
	struct hash<nJinn::gameObject> {
		typedef nJinn::gameObject argument_type;
		typedef std::size_t result_type;
		result_type operator()(const nJinn::gameObject & t) const{ return (size_t)&t; }
	};
}

namespace nJinn {
	class gameObject
	{
	private:
		Eigen::Vector3d mPosition;
		Eigen::Vector3d mScale;
		Eigen::Quaterniond mRotation;

		bool mTransformChanged;
		void markTransformChanged() {
			if (!mTransformChanged) {
				mTransformChanged = true;
				for (auto child : mChildren)
					child->markTransformChanged();
			}
		}
		Eigen::Matrix4d mTransform;

		gameObject* mParent;
		std::vector<gameObject*> mChildren;

		std::vector<component*> mComponents;

	public:
		gameObject();
		gameObject(const gameObject & original);
		~gameObject();

		static gameObject * create();

		template<typename component_t>
		component_t * addComponent();

		template<typename component_t>
		component_t * getComponent();

		gameObject * addChild();

		void parent(gameObject * value);
		gameObject * parent() const { return mParent; }

		void destroyNow();

		gameObject * copy();

		const Eigen::Vector3d position() const { return mPosition; }
		void position(const Eigen::Vector3d & value) { mPosition = value; markTransformChanged(); }
		void position(double x, double y, double z) { mPosition << x, y, z; markTransformChanged(); }

		const Eigen::Vector3d scale() const { return mScale; }
		void scale(const Eigen::Vector3d & value) { mScale = value; markTransformChanged(); }
		void scale(double x, double y, double z) { mScale << x, y, z; markTransformChanged();  }

		const Eigen::Quaterniond rotation() const { return mRotation; }
		void rotation(const Eigen::Quaterniond & value) { mRotation = value; mRotation.normalize(); markTransformChanged(); }

		const Eigen::Matrix4d & transform();

		bool operator==(const gameObject & other) const {
			return this == &other;
		}
	private:
		typedef std::unordered_set<gameObject, std::hash<gameObject>, std::equal_to<gameObject>, Eigen::aligned_allocator<gameObject>> set_type;
		static set_type sGameObjects;

		friend class context;
		friend class application;
		static void finalize();
	};


	template<typename component_t>
	inline component_t * gameObject::addComponent()
	{
		component_t * comp = new component_t(this);
		mComponents.push_back(comp);
		return comp;
	}

	template<typename component_t>
	inline component_t * gameObject::getComponent()
	{
		for (component * c : mComponents) {
			component_t * ret = dynamic_cast<component_t>(c);
			if (c) return ret;
		}
		return nullptr;
	}

}
