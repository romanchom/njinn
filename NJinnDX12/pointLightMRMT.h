#pragma once
#include "pointLightMR.h"

namespace nJinn {
	class pointLightMRMT : public pointLightMR
	{
	private:
		class shadowTask : public renderingTask {
		public:
			shadowTask(pointLightMRMT * light, uint32_t index);
			virtual void execute() override;
		private:
			pointLightMRMT * mLight;
			uint32_t mIndex;
		};

		void doGenerateShadowMap(ID3D12GraphicsCommandList * list, uint32_t index);

		shadowTask mShadowTasks[6];
	protected:
		virtual void update() override;
		virtual void generateShadowMap() override;

		gpuStopwatch mStopwatch;
	public:
		pointLightMRMT(gameObject * owner);
	};
}

