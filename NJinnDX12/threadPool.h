#pragma once

#include <queue>
#include <thread>
#include <condition_variable>
#include <mutex>
#include "task.h"

namespace nJinn {
	class threadPool
	{
	private:
		uint32_t mWorkerCount;
		std::thread * mWorkers;
		std::queue<task *> mTaskQueue;
		std::mutex mMutex;
		std::condition_variable mConditionVariable;
		std::condition_variable mConditionVariableIdle;
		volatile bool mShouldRun;
		volatile uint32_t mIdleThreads;
		void workerFunction();
	public:
		threadPool(uint32_t workerCount);
		~threadPool();
		void submitTask(task * t);
		void waitUntillCompleted();
	};
}
