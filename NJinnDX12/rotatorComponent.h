#pragma once
#include "component.h"
#include "stopwatch.h"

namespace nJinn {
	class rotatorComponent : public component
	{
		stopwatch t;
		double offset;
	public:
		rotatorComponent(class gameObject * owner);
		virtual rotatorComponent * copyTo(class gameObject * target) const override;
	protected:
		virtual void initialize() override {};
		virtual void update() override;
	};
}
