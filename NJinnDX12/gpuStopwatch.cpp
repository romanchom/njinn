#include "stdafx.h"
#include "gpuStopwatch.h"

#include "config.h"
#include "dx12helper.h"
#include "context.h"

namespace nJinn {
	gpuStopwatch::gpuStopwatch(size_t measurementCount) :
		mQueryCount(measurementCount),
		mCurrent(0)
	{
		{
			D3D12_QUERY_HEAP_DESC desc = {};
			desc.Count = measurementCount * config::backBufferCount;
			desc.NodeMask = 0;
			desc.Type = D3D12_QUERY_HEAP_TYPE_TIMESTAMP;
			DC(device->CreateQueryHeap(&desc, IID_PPV_ARGS(&mQueryHeap)));
		}
		{
			CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(measurementCount * config::backBufferCount * 8, D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE, 0);

			DC(device->CreateCommittedResource(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_READBACK),
				D3D12_HEAP_FLAG_NONE,
				&desc,
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&mDestinationBuffer)));
		}

		mResults = new uint64_t[measurementCount];
		mCPUResults = new uint64_t[measurementCount];
		LARGE_INTEGER f;
		QueryPerformanceFrequency(&f);
		mCPUFrequency = f.QuadPart;
	}

	gpuStopwatch::~gpuStopwatch()
	{
		delete[] mResults;
	}

	void gpuStopwatch::measure(size_t slot, ID3D12GraphicsCommandList * list)
	{
		list->EndQuery(mQueryHeap.Get(), D3D12_QUERY_TYPE_TIMESTAMP, slot + mQueryCount * mCurrent);
		QueryPerformanceCounter((LARGE_INTEGER *) (mCPUResults + slot));
	}

	double gpuStopwatch::getDifference(size_t begin, size_t end)
	{
		return (mResults[end] - mResults[begin]) / ctx->GPUTimerFrequency();
	}

	double gpuStopwatch::getCPUDifference(size_t begin, size_t end)
	{
		return (mCPUResults[end] - mCPUResults[begin]) / mCPUFrequency;
	}

	void gpuStopwatch::reset(ID3D12GraphicsCommandList * list)
	{
		mCurrent = ctx->frameIndex();

		void * ptr;
		D3D12_RANGE r = { 0, mQueryCount * 8 };
		DC(mDestinationBuffer->Map(0, &r, &ptr));
		memcpy(mResults, ptr, 8 * mQueryCount);
		r = { 0, 0 };
		mDestinationBuffer->Unmap(0, &r);

		list->ResolveQueryData(
			mQueryHeap.Get(),
			D3D12_QUERY_TYPE_TIMESTAMP,
			mQueryCount * mCurrent,
			mQueryCount,
			mDestinationBuffer.Get(),
			mCurrent * mQueryCount * 8);
	}
}