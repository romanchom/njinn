#pragma once

#include "texture.h"
#include <dxgi1_4.h>

namespace nJinn {
	class renderTexture : public texture
	{
	private:
		D3D12_RENDER_TARGET_VIEW_DESC mTargetViewDescriptor;
		DXGI_FORMAT mFormat;
		float mClearValue[4];
	public:
		renderTexture(uint32_t width, uint32_t height, DXGI_FORMAT format, float clearValue[4] = nullptr, uint32_t count = 1);
		renderTexture(uint32_t width, DXGI_FORMAT format, float clearValue[4] = nullptr);
		void createRenderTargetViewDescriptor(D3D12_CPU_DESCRIPTOR_HANDLE handle);
		float * clearValue() { return mClearValue; }
	};

	typedef std::shared_ptr<renderTexture> pRenderTexture;
}