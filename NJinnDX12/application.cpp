#include "stdafx.h"
#include "application.h"

#include "context.h"
#include "window.h"
#include "input.h"
#include "gameTime.h"
#include "component.h"
#include "gameObject.h"
#include "deferredRenderer.h"
#include "stopwatch.h"
#include <comdef.h>

namespace nJinn {
	gameBase * application::mGame = nullptr;
	HINSTANCE application::shInstance = nullptr;
	int application::snCmdShow = 1;
	window * application::sWindow = nullptr;

	void nJinn::application::doInitialize(HINSTANCE hInstance, const char * args)
	{
		config::initialize(args);
		shInstance = hInstance;
		sWindow = new window(1600, 900);
		context::initialize(sWindow);
		run();
	}

	void application::quit()
	{
		PostQuitMessage(0);
	}

	void nJinn::application::run()
	{
		mGame->OnInitialize();
		stopwatch w;
		w.reset();
		int i = 0;
		while (true) {
			if (sWindow->run() == -1) break;

			gameTime::update();
			mGame->OnUpdate();
			component::initializeNew();
			component::updateComponents();

			mGame->OnPreRender();
			ctx->deferredRenderer.draw();
			mGame->OnRendered();

			ctx->frameResources.present();

			if (++i == 10) {
				std::wostringstream os;
				os << i / w.seconds() << "FPS\n";
				//sWindow->title(os.str());
				i = 0;
				w.reset();
			}
		}
		finalize();
	}

	void application::finalize()
	{
		mGame->OnExit();
		delete mGame;
		delete sWindow;
		context::finalize();
	}
}