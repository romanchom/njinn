#pragma once

namespace nJinn {
	class gameBase {
	public:
		virtual void OnInitialize() {}
		virtual void OnUpdate() {}
		virtual void OnPreRender() {}
		virtual void OnRendered() {}
		virtual void OnExit() {}
	};

	class application
	{
	public:
		template<typename T>
		static int initialize(HINSTANCE hInstance, const char * args);
		static void quit();
	private:
		static void run();
		static void finalize();
		application() = delete;
		static void doInitialize(HINSTANCE hInstance, const char * args);
		static gameBase * mGame;
		static HINSTANCE shInstance;
		static int snCmdShow;
		static class window * sWindow;

		friend class window;
	};

	template<typename T>
	inline int application::initialize(HINSTANCE hInstance, const char * args)
	{
		if (mGame) throw;
		mGame = new T();
		doInitialize(hInstance, args);
		return 0;
	}
}
