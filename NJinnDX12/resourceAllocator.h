#pragma once

#include <d3d12.h>
#include <deque>

#include "config.h"

namespace nJinn {
	class resourceAllocator
	{
	public:
		static void allocate(Microsoft::WRL::ComPtr<ID3D12Resource>& ret, D3D12_RESOURCE_STATES state, size_t sizeInBytes, void * data);
		static void allocate(Microsoft::WRL::ComPtr<ID3D12Resource>& ret, const D3D12_RESOURCE_DESC & desc, D3D12_RESOURCE_STATES state, Microsoft::WRL::ComPtr<ID3D12Resource>& tmp);
	private:
		struct uploadTask {
			Microsoft::WRL::ComPtr<ID3D12Resource> src;
			Microsoft::WRL::ComPtr<ID3D12Resource> dst;
		};
		static Microsoft::WRL::ComPtr<ID3D12CommandAllocator> mCommandAllocators[config::backBufferCount];
		static Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> mCommandList;
		static std::deque<uploadTask> mPendingTasks;
		static unsigned int mTasksProcessed;
		static unsigned int mTasksProcessedPreviously;

		static void initialize();
		static void release();
		static void executeUpload();
		static void reset();

		friend class nJinnDevice;
		friend class context;
		friend class deferredRenderer;
	};
}