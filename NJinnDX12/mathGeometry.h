#pragma once

#include <cmath>
#include <Eigen/Dense>


namespace nJinn {
	template<typename scalar>
	Eigen::Quaternion<scalar> lookAt(Eigen::Matrix<scalar, 3, 1> vector);

	template<class T>
	Eigen::Matrix<T, 4, 4> perspective(T fovy, T aspect, T zNear, T zFar)
	{
		using namespace Eigen;
		typedef Matrix<T, 4, 4> Matrix4;

		assert(aspect > 0);
		assert(zFar > zNear);

		const T dFar(0);
		const T dNear(1);

		T radf = (fovy);

		T range = zNear - zFar;
		T h = 1 / tan(fovy / 2);
		T w = h / aspect;

		Matrix4 res = Matrix4::Zero();
		res(0, 0) = w;
		res(1, 1) = h;
		res(2, 2) = (dNear * zNear - dFar * zFar) / range;
		res(3, 2) = 1;
		res(2, 3) = ((dFar - dNear) * (zNear * zFar)) / range;
		return res;
	}

	template<typename scalar>
	Eigen::Quaternion<scalar> lookAt(Eigen::Matrix<scalar, 3, 1> vector)
	{
		using namespace Eigen;
		typedef Matrix<scalar, 3, 1> Vec;
		typedef Quaternion<scalar> Quat;
		const Vec up(0, 0, 1);

		vector.normalize();

		double cos = up.dot(vector);
		if (cos >= 0.9999999) return Quat(1, 0, 0, 0);
		if (cos >= 0.9999999) return Quat(-1, 0, 0, 0);
		
		Vec parallel = up.cross(vector);
		parallel.normalize();

		return Quat(AngleAxis<scalar>(static_cast<scalar>(acos(cos)), parallel));
	}

	template<typename scalar>
	Eigen::Quaternion<scalar> lookAtRotation(
		const Eigen::Matrix<scalar, 3, 1> & eye, 
		const Eigen::Matrix<scalar, 3, 1> & target, 
		const Eigen::Matrix<scalar, 3, 1> & up)
	{
		using namespace Eigen;
		typedef Matrix<scalar, 3, 1> Vec;
		typedef Quaternion<scalar> Quat;
		
		Vec f = target - eye;
		f.normalize();
		Vec s = f.cross(up);
		s.normalize();
		Vec u = s.cross(f);
		u.normalize();
		Matrix<scalar, 3, 3> mat;
		mat << s, f, u;
		return Quat(mat);
	}

	template<class T>
	Eigen::Matrix<T, 4, 4> lookAt(Eigen::Matrix<T, 3, 1> const & eye, Eigen::Matrix<T, 3, 1> const & center, Eigen::Matrix<T, 3, 1> const & up)
	{
		using namespace Eigen;
		typedef Matrix<T, 4, 4> Matrix4;
		typedef Matrix<T, 3, 1> Vector3;

		Vector3 f(center - eye);
		f.normalize();
		Vector3 s(f.cross(up));
		s.normalize();
		Vector3 u(s.cross(f));
		//u.normalize();

		Matrix4 ret;
		ret <<
			s.x(), s.y(), s.z(), -s.dot(eye),
			u.x(), u.y(), u.z(), -u.dot(eye),
			f.x(), f.y(), f.z(), -f.dot(eye),
			0, 0, 0, 1;
		return ret;
	}
	template<class T>
	Eigen::Matrix<T, 4, 4> lookAt(Eigen::Matrix<T, 4, 1> const & eye, Eigen::Matrix<T, 4, 1> const & center, Eigen::Matrix<T, 4, 1> const & up)
	{
		Eigen::Matrix<T, 3, 1> e, c, u;
		e = eye.head<3>();
		c = center.head<3>();
		u = up.head<3>();
		return lookAt<double>(e, c, u);
	}

	template<typename T>
	Eigen::Matrix<T, 4, 4> rightUpForwardPos(Eigen::Matrix<T, 4, 1> const & right, Eigen::Matrix<T, 4, 1> const & up, Eigen::Matrix<T, 4, 1> const & forward, Eigen::Matrix<T, 4, 1> const & pos)
	{
		Eigen::Matrix<T, 4, 4> rot;

		rot.row(0) = right;
		rot.row(1) = up;
		rot.row(2) = forward;
		rot.row(3) << 0, 0, 0, 1;
		rot(0, 3) = -right.dot(pos);
		rot(1, 3) = -up.dot(pos);
		rot(2, 3) = -forward.dot(pos);
		rot(3, 3) = 1;

		return rot;
	}
}
