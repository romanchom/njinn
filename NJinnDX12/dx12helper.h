#pragma once

#include "d3dx12.h"

#ifdef _DEBUG
#include <exception>
#include <iostream>
#include <comdef.h>

#define STRINGIFY(x) STRINGIFY2(x)
#define STRINGIFY2(x) #x
#define LINE_STRING STRINGIFY(__LINE__)

// stands for debug check
#define DC(result) { \
	HRESULT res = (result); \
	if(FAILED(res)) { \
		_com_error err(res); \
		LPCTSTR errMsg = err.ErrorMessage(); \
		std::cout << "Error: " << errMsg << std::endl; \
		std::cout << "Operation failed: " __FILE__ " : " LINE_STRING << std::endl; \
		throw std::exception("Operation failed: " __FILE__ " : " LINE_STRING); \
	} \
}

#define SET_NAME(object,name) object->SetName(name)


#else
#define DC(result) (result)
#define SET_NAME(object,name)

#endif

#define DBOUT( s )            \
{                             \
   std::wostringstream os_;    \
   os_ << s;                   \
   OutputDebugString( os_.str().c_str() );  \
}