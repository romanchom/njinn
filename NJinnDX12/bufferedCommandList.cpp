#include "stdafx.h"
#include "bufferedCommandList.h"

#include "context.h"
#include "dx12helper.h"

namespace nJinn {
	bufferedCommandList::bufferedCommandList()
	{
		for (int i = 0; i < config::backBufferCount; ++i) {
			DC(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&(mCommandAllocators[i]))));
		}
		ID3D12CommandAllocator * alloc = mCommandAllocators[0].Get();
		DC(device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, alloc, nullptr, IID_PPV_ARGS(&mCommandList)));
		mCommandList->Close();
	}

	void bufferedCommandList::reset()
	{
		ID3D12CommandAllocator * alloc = mCommandAllocators[ctx->frameIndex()].Get();
		DC(alloc->Reset());
		DC(mCommandList->Reset(alloc, nullptr));
	}

	void bufferedCommandList::close()
	{
		DC(mCommandList->Close());
	}
}