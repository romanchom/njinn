#pragma once

#include <Eigen/Dense>
#include "lightSource.h"
#include "shader.h"
#include "renderTarget.h"
#include "depthStencilTexture.h"

namespace nJinn {
	class directionalLight : public lightSource
	{
	protected:
		virtual void initialize() override;
		virtual void update() override;
		virtual void illuminate(ID3D12GraphicsCommandList * list) override;
		virtual void generateShadowMap() override;
		virtual void setGBufferTextures(renderTarget & rt) override;

		pShader mVertexShader;
		pShader mPixelShader;
		Microsoft::WRL::ComPtr<ID3D12PipelineState> mPipelineState;

		descriptorHeap mTextureViewHeap;
		
	public:
		directionalLight(class gameObject * owner);

		virtual directionalLight * copyTo(gameObject * target) const override;

		void color(const Eigen::Vector3f & color);// { mConstants.color.topLeftCorner(3, 1) = color; }
		const Eigen::Vector3f & color();// { return mConstants.color.topLeftCorner(3, 1); }

		//TODO FIXME PLEASE
	};
}
