#include "NormalCodec.hlsli"
#include "DepthDecode.hlsli"

#include "frameConstants.h"
#include "shadowConstants.h"
#include "pointLightConstants.h"


TextureCube<float> shadowMap : register(t0);
Texture2D albedoTex : register(t1);
Texture2D<float2> normalTex : register(t2);
Texture2D<float> depthTex : register(t3);

sampler mipSampler : register(s0);
sampler pointSampler : register(s1);
SamplerComparisonState shadowSampler : register(s2);

float4 main(float4 screenPos : SV_POSITION) : SV_TARGET
{
	int3 uv = (int3) screenPos.xyz;
	float depth = decodeDepthApprox1(depthTex.Load(uv), frame.nearPlane, frame.farPlane);
	float2 viewDir = frame.screenPosMul * screenPos.xy + frame.screenPosAdd;
	float3 viewSpacePos = float3(depth * viewDir, depth);

	float3 lightRay = light.center.xyz - viewSpacePos;

	float3 absDiff = abs(lightRay);
	float shadowZ = max(absDiff.x, max(absDiff.y, absDiff.z));
	shadowZ *= 0.98;
	shadowZ = (shadow.nearMulFar - shadow.nearPlane * shadowZ) / ((shadow.farMinNear) * shadowZ);

	float shadow = shadowMap.SampleCmp(shadowSampler, -lightRay, shadowZ);
	if (shadow <= 0) discard;

	float distSquared = dot(lightRay, lightRay);
	lightRay *= rsqrt(distSquared);

	float3 normal = decodeNormal(normalTex.Load(uv));

	float nDotL = dot(normal, lightRay);

	clip(nDotL);

	float4 albedo = albedoTex.Load(uv);
	float3 halfWayLightView = normalize(lightRay - normalize(viewSpacePos));
	float specular = max(0, dot(normal, halfWayLightView));
	specular = pow(specular, 100);
	return float4(shadow / distSquared * light.color * (specular + albedo.xyz * nDotL), albedo.a);
}