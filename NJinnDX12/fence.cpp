#include "stdafx.h"
#include "fence.h"
#include "context.h"

namespace nJinn {
	fence::fence() :
		mQueue(nullptr),
		mEvent(nullptr)
	{}

	fence::fence(ID3D12CommandQueue * queue) :
		mValue(0),
		mQueue(queue)
	{
		DC(device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(mFence.GetAddressOf())));

		mEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (mEvent == nullptr) {
			DC(HRESULT_FROM_WIN32(GetLastError()));
		}
	}

	fence::~fence()
	{
		if(mEvent) CloseHandle(mEvent);
	}

	void fence::signal(uint64_t value)
	{
		//++mValue;
		DC(mQueue->Signal(mFence.Get(), value));
	}

	void fence::wait(uint64_t value)
	{
		uint64_t com = mFence->GetCompletedValue();

		if (com < value) {
			DC(mFence->SetEventOnCompletion(value, mEvent));
			WaitForSingleObjectEx(mEvent, INFINITE, 0);
		}
	}
}