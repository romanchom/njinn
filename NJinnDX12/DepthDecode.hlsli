float decodeDepth01Exact(float depth, float near, float far) {
	return near / (near + depth * (far - near));
}

float decodeDepth01Approx1(float depth, float near, float far) {
	return near / (near + depth * far);
}

float decodeDepth01Approx2(float depth, float near, float far) {
	return near / (depth * far);
}

float decodeDepthExact(float depth, float near, float far) {
	return decodeDepth01Exact(depth, near, far) * far;
}

float decodeDepthApprox1(float depth, float near, float far) {
	return decodeDepth01Approx1(depth, near, far) * far;
}

float decodeDepthApprox2(float depth, float near, float far) {
	return near / depth;
}