struct input {
	float3 pos : POSITION;
};

struct output {
	float4 pos : SV_POSITION;
	float3 viewDir : VIEW_DIR;
};

cbuffer frameConstants : register(b0)
{
	float4x4 view;
	float4x4 projection;
	float4x4 viewProjection;
	float4x4 invProjection;
	float4 viewDir;
	float nearPlane;
	float farPlane;
};

output main(input i)
{
	output o;
	o.pos = float4(i.pos, 1);
	o.viewDir = viewDir.xyz * float3(o.pos.xy, 1);
	return o;
}