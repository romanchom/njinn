#include "stdafx.h"
#include "rand.h"


namespace nJinn {

	std::default_random_engine rand::sEngine;
	double rand::uniform(double min, double max)
	{
		std::uniform_real_distribution<double> dist(min, max);
		return dist(sEngine);
	}


}