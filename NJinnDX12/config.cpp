#include "stdafx.h"
#include "config.h"

#include <vector>
#include <string>

using namespace boost::program_options;

namespace nJinn {
	std::wstring config::mAssetPath;
	options_description config::optionsDesc("Allowed options");
	variables_map config::options;

	void config::initialize(const char * args) {
		const size_t pathSize = 1024;
		wchar_t path[pathSize];

		size_t size = GetModuleFileName(nullptr, path, pathSize);

		if (size == 0 || size == pathSize) {
			throw std::runtime_error("Could not get asset path.");
		}

		WCHAR* lastSlash = wcsrchr(path, L'\\');
		if (lastSlash)
		{
			*(lastSlash + 1) = NULL;
		}
		wcscat(path, L"assets\\");

		mAssetPath = path;

		optionsDesc.add_options()
			("threads", value<uint32_t>()->default_value(1), "thread pool threads")
			("shadowMapRes", value<uint32_t>()->default_value(256), "shadow map resolution");

		std::vector<std::string> arguments = split_winmain(args);

		store(command_line_parser(arguments).options(optionsDesc).run(), options);
		notify(options);
	}

	uint32_t config::shadowMapResolution()
	{
		return options["shadowMapRes"].as<uint32_t>();
	}

	uint32_t config::threadCount()
	{
		return options["threads"].as<uint32_t>();
	}
}