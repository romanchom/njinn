#include "stdafx.h"
#include "lightSource.h"

#include "renderer.h"
#include "context.h"

namespace nJinn {
	std::unordered_set<lightSource *> lightSource::sLights;
	
	lightSource::lightSource(gameObject * owner) : 
		component(owner, 102)
	{
		sLights.insert(this);
	}

	lightSource::~lightSource()
	{
		sLights.erase(this);
	}

	void lightSource::initialize()
	{
		this->setGBufferTextures(ctx->deferredRenderer.mGBuffer);
	}

	void lightSource::drawAllShadowMaps()
	{
		for (auto light : sLights) {
			light->generateShadowMap();
		}
	}

	void lightSource::drawAll(ID3D12GraphicsCommandList * list)
	{
		for (auto light : sLights) {
			light->illuminate(list);
		}
	}

	void lightSource::drawShadowCasters(ID3D12GraphicsCommandList * list)
	{
		renderer::drawDepthOnlyAll(list);
	}

	void lightSource::drawShadowCastersGS(ID3D12GraphicsCommandList * list)
	{
		renderer::drawDepthOnlyGSAll(list);
	}

	void lightSource::drawShadowCastersInstanced(ID3D12GraphicsCommandList * list, size_t instanceCount)
	{
		renderer::drawDepthOnlyInstancedAll(list, instanceCount);
	}
}