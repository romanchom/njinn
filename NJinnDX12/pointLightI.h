#pragma once
#include "pointLight.h"
#include "renderTarget.h"
#include "gpuStopwatch.h"
#include "renderingTask.h"
#include <d3d12.h>

namespace nJinn {
	class pointLightI : public pointLight
	{
	private:
		class shadowTask : public renderingTask {
		public:
			shadowTask(pointLightI * light);
			virtual void execute() override;
		private:
			pointLightI * mLight;
		};

		void doGenerateShadowMap(ID3D12GraphicsCommandList * list);
		D3D12_VERTEX_BUFFER_VIEW mInstanceDataView;
		renderTarget mRenderTarget;
		shadowTask mShadowTask;

		gpuStopwatch mStopwatch;
	protected:
		virtual void update() override;
		virtual void generateShadowMap() override;
	public:
		pointLightI(gameObject * owner);
	};
}
