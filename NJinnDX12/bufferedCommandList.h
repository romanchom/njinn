#pragma once

#include <d3d12.h>

#include "config.h"

namespace nJinn {
	class bufferedCommandList
	{
	public:
		bufferedCommandList();
		void reset();
		void close();
		ID3D12GraphicsCommandList * get() { return mCommandList.Get(); }
		ID3D12GraphicsCommandList * operator->() { return get(); }
		operator ID3D12GraphicsCommandList *() { return get(); }
	private:
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> mCommandList;
		Microsoft::WRL::ComPtr<ID3D12CommandAllocator> mCommandAllocators[config::backBufferCount];
	};
}
