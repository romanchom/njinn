#include "stdafx.h"
#include "context.h"

#include <d3d12.h>
#include "dx12helper.h"

#include "window.h"
#include "gameObject.h"
#include "commandBundle.h"
#include "mesh.h"
#include "shader.h"
#include "texture.h"
#include "resourceAllocator.h"
#include "application.h"
using namespace Microsoft::WRL;

namespace nJinn {
	ID3D12Device * device = nullptr;
	context * ctx = nullptr;
	
	void context::initialize(window * win)
	{
		createDebugConsole();
		createDevice();
		ctx = new context(win);
		ctx->profiler.initialize();
		resourceAllocator::initialize();
		commandBundle::initialize();
		mesh::initialize();
	}

	void context::finalize()
	{
		ctx->frameResources.synchronize();
		gameObject::finalize();
		mesh::finalize();
		commandBundle::finalize();
		mesh::collect();
		shader::collect();
		texture::collect();
		resourceAllocator::release();
		delete ctx;
		destroyDevice();
	}

	void context::createDevice()
	{
#if defined(_DEBUG) || defined(_QUERY)
		{
			ComPtr<ID3D12Debug> debugController;
			if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
				debugController->EnableDebugLayer();
		}
#endif
		DC(D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&device)));

#ifdef _QUERY
		DC(device->SetStablePowerState(true));
#endif
	}

	void context::destroyDevice()
	{
#ifdef _DEBUG
		ID3D12DebugDevice * dd;
		device->QueryInterface<ID3D12DebugDevice>(&dd);
		dd->ReportLiveDeviceObjects(D3D12_RLDO_DETAIL);
		dd->Release();
#endif
		device->Release();
	}
	
	void context::createDebugConsole()
	{
#if defined(_DEBUG) || defined(_QUERY)
		AllocConsole();
		std::wstring strW = L"Debug Console";
		SetConsoleTitle(strW.c_str());

		freopen("CONOUT$", "wt", stdout);
#endif
	}

	context::context(window * win) :
		mWindow(win),
		mainCommandQueue(D3D12_COMMAND_LIST_TYPE_DIRECT),
		copyCommandQueue(D3D12_COMMAND_LIST_TYPE_COPY),
		frameResources(mainCommandQueue, win->mWindowHandle, win->mWidth, win->mHeight)
	{
		SET_NAME(mainCommandQueue, L"Main graphics command queue");
		SET_NAME(copyCommandQueue, L"Copy command queue");

#ifdef _QUERY
		uint64_t t;
		DC(mainCommandQueue->GetTimestampFrequency(&t));
		mGPUTimerFrequency = (double) t;
#endif
	}
}