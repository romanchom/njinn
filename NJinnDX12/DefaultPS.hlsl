#include "NormalCodec.hlsli"

struct input {
	float3 normal : NORMAL;
	float2 uv : TEX_COORD0;
};

Texture2D tex : register(t0);
sampler sampl : register(s0);


struct output {
	float4 albedo : SV_Target0;
	float4 normal : SV_Target1;
};

output main(input i)
{
	output o;
	o.albedo = tex.Sample(sampl, i.uv);
	o.normal = float4(encodeNormal(normalize(i.normal)), 0, 1);
	return o;
}