#include "stdafx.h"
#include "meshRenderer.h"

#include "mesh.h"
#include "shader.h"
#include "d3dx12.h"
#include "context.h"
#include "dx12helper.h"
#include "gameObject.h"
#include "camera.h"
#include "texture.h"
#include "geometryRootSig.h"

namespace nJinn {
	using namespace Eigen;
	meshRenderer::meshRenderer(gameObject * owner) : 
		renderer(owner),
		mTextureViewHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, 5)
	{
		mPixelShader = shader::load({ L"DefaultPS", pixel });
		mVertexShader = shader::load({ L"DefaultVS", vertex });
		mTexture = texture::load(L"white.dds");

		mShadowPixelShader = shader::load({ L"ShadowPS", pixel });

		mVS0 = shader::load({ L"Shadow0VS", vertex });

		mGS1 = shader::load({ L"Shadow1GS", geometry });
		mVS1 = shader::load({ L"Shadow1VS", vertex });

		mVS2 = shader::load({ L"Shadow2VS", vertex });
	}

	meshRenderer::~meshRenderer()
	{}

	void meshRenderer::draw(ID3D12GraphicsCommandList * list)
	{
		ID3D12DescriptorHeap * heap = mTextureViewHeap.get();
		list->SetDescriptorHeaps(1, &heap);
		list->SetGraphicsRootConstantBufferView(geometryRootSig::perObjectCBLocation, mConstBuffAddr);
		list->SetGraphicsRootDescriptorTable(geometryRootSig::perObjectTexturesLocation, mTextureViewHeap.getGPUHandle());
		list->SetPipelineState(mPipelineState.Get());
		mMesh->draw(list);
	}

	void meshRenderer::drawDepthOnly(ID3D12GraphicsCommandList * list)
	{
		list->SetGraphicsRootConstantBufferView(geometryRootSig::perObjectCBLocation, mConstBuffAddr);
		list->SetPipelineState(mPSO0.Get());
		mMesh->draw(list);
	}

	void meshRenderer::drawDepthOnlyGS(ID3D12GraphicsCommandList * list)
	{
		list->SetGraphicsRootConstantBufferView(geometryRootSig::perObjectCBLocation, mConstBuffAddr);
		list->SetPipelineState(mPSO1.Get());
		mMesh->draw(list);
	}

	void meshRenderer::drawDepthOnlyInstanced(ID3D12GraphicsCommandList * list, size_t instanceCount)
	{
		list->SetGraphicsRootConstantBufferView(geometryRootSig::perObjectCBLocation, mConstBuffAddr);
		list->SetPipelineState(mPSO2.Get());
		mMesh->draw(list, 6);
	}

	meshRenderer * meshRenderer::copyTo(class gameObject * target) const
	{
		meshRenderer * ret = new meshRenderer(target);
		ret->mVertexShader = mVertexShader;
		ret->mPixelShader = mPixelShader;
		ret->mMesh = mMesh;
		ret->mTexture = mTexture;
		return ret;
	}

	void meshRenderer::initialize()
	{
		mTexture->createShaderView(mTextureViewHeap.getCPUHandle());
		{
			D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = ctx->deferredRenderer.mGeometryPSODesc; // TODO accessor
			// package it into a material
			mMesh->fillPSO(psoDesc);
			psoDesc.VS = *mVertexShader;
			psoDesc.PS = *mPixelShader;

			DC(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&mPipelineState)));
		}
		// simple, multipass
		{
			D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = ctx->deferredRenderer.mShadowPSODesc; // TODO accessor
			mMesh->fillPSO(psoDesc);
			psoDesc.VS = *mVS0;

			DC(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&mPSO0)));
		}
		// geometry shader
		{
			D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = ctx->deferredRenderer.mShadowPSODesc; // TODO accessor
			mMesh->fillPSO(psoDesc);
			psoDesc.VS = *mVS1;
			psoDesc.GS = *mGS1;

			DC(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&mPSO1)));
		}
		// instancing
		{
			D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = ctx->deferredRenderer.mShadowPSODesc; // TODO accessor
			mMesh->fillPSO(psoDesc);
			D3D12_INPUT_ELEMENT_DESC desc[10];
			uint32_t attrc = psoDesc.InputLayout.NumElements;
			memcpy(desc, psoDesc.InputLayout.pInputElementDescs, sizeof(desc));
			
			D3D12_INPUT_ELEMENT_DESC attr;
			attr.SemanticName = "VP";
			attr.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			attr.InputSlot = 1;
			attr.InputSlotClass = D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA;
			attr.InstanceDataStepRate = 1;
			for (int i = 0; i < 4; ++i) {
				attr.SemanticIndex = i;
				attr.AlignedByteOffset = i * 16;
				desc[attrc + i] = attr;
			}
			attrc += 4;
			psoDesc.InputLayout.pInputElementDescs = desc;
			psoDesc.InputLayout.NumElements = attrc;
			psoDesc.VS = *mVS2;

			DC(device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&mPSO2)));
		}
	}

	void meshRenderer::update()
	{
		auto cbuff = constBuff::create<objectConstants>();

		const Matrix4d & model = mOwner->transform();
		Matrix4d modelView = camera::main()->viewMatrix() * model;
		Matrix4d modelViewProjection = camera::main()->viewProjectionMatrix() * model;
		//Matrix4d invTModelView = camera::main()->viewMatrix() * model.inverse().transpose();
		Matrix4d invTModelView = modelView.inverse().transpose();

		cbuff.cpuP->model = model.cast<float>();
		cbuff.cpuP->modelView = modelView.cast<float>();
		cbuff.cpuP->modelViewProjection = modelViewProjection.cast<float>();
		cbuff.cpuP->invTModelView = invTModelView.cast<float>();

		mConstBuffAddr = cbuff.gpuP;
	}
}