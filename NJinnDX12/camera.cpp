#include "stdafx.h"
#include "camera.h"
#include "gameObject.h"
#include <cmath>
#include "mathGeometry.h"

namespace nJinn {
	using namespace Eigen;

	camera * camera::sMainCamera = nullptr;

	camera::camera(gameObject * owner) : component(owner, 100),
		mNear(0.1),
		mFar(1000),
		mFieldOfView(1)
	{
		if (sMainCamera == nullptr) sMainCamera = this;
	}

	void camera::initialize()
	{
	}

	void camera::update()
	{
		Matrix4d fix;
		fix << 1, 0, 0, 0,
			   0, 0, 1, 0,
			   0, 1, 0, 0,
			   0, 0, 0, 1;
		Matrix4d rotation;
		rotation.setIdentity();
		rotation.topLeftCorner<3, 3>() = mOwner->rotation().inverse().toRotationMatrix();
		
		Matrix4d translation;
		translation.setIdentity();
		translation.col(3) << -mOwner->position(), 1;

		mView = fix * rotation * translation;
		//mView = lookAt<double>(Vector3d::Zero(), Vector3d(0, 1, 0), Vector3d::UnitZ());
		mProjection = perspective<double>(mFieldOfView, 16.0 / 9, mNear, mFar);
		mViewProjection = mProjection *mView;
	}

	camera * camera::copyTo(class gameObject * target) const
	{
		camera * ret = new camera(target);
		return ret;
	}
}