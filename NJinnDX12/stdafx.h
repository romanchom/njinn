#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 
#endif

#define NOMINMAX

#include <windows.h>


#include <string>
#include <wrl.h>
#include <Eigen/Dense>
#include <vector>
#include <memory>
#include <cstdint>