#pragma once

namespace win {
	class memMapFile
	{
	private:
		HANDLE mFile;
		HANDLE mFileMapping;
		size_t mFileSize;
		void * mPointer;
	public:
		memMapFile(const std::wstring & fileName);
		~memMapFile();
		void * get(size_t offset = 0) const { return (char *) mPointer + offset; }
	};
}