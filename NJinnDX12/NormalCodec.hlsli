float2 encodeNormal(float3 input) {
	float2 enc = normalize(input.xy) * sqrt(input.z * 0.5 + 0.5);
	return enc;
}

float3 decodeNormal(float2 input) {
	float l = dot(input, input);
	float3 n;
	n.z = l * 2 - 1;
	n.xy = input.xy * rsqrt(l);
	n.xy *= sqrt(1 - n.z * n.z);
	return n;
}