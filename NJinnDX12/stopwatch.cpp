#include "stdafx.h"
#include "stopwatch.h"


stopwatch::stopwatch()
{
	QueryPerformanceFrequency(&mFrequency);
	reset();
}

void stopwatch::reset()
{
	QueryPerformanceCounter(&mStart);
}

double stopwatch::seconds()
{
	LARGE_INTEGER stop;
	QueryPerformanceCounter(&stop);
	double diff = (double) (stop.QuadPart - mStart.QuadPart);
	diff /= mFrequency.QuadPart;

	return diff;
}
