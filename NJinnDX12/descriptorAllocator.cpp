#include "stdafx.h"
#include "descriptorAllocator.h"
#include "context.h"
#include "dx12helper.h"


namespace nJinn {
	descriptorAllocator::descriptorAllocator(D3D12_DESCRIPTOR_HEAP_TYPE type, size_t initialSize) : 
		mHeap(type, initialSize),
		mSlotsTaken(0)
	{}

	descriptorTable descriptorAllocator::allocate(size_t slots)
	{
		// TODO: implement proper memory management
		if (mSlotsTaken + slots > mHeap.size()) throw std::runtime_error("Unable to allocate descriptor table. Increase the size or better yet implement proper memory management.");
		descriptorTable ret;
		ret.CPUHandle = mHeap.getCPUHandle(mSlotsTaken);
		ret.GPUHandle = mHeap.getGPUHandle(mSlotsTaken);
		ret.size = slots;
		mSlotsTaken += slots;
		return ret;
	}
}