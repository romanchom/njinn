#include "stdafx.h"
#include "input.h"

#include <windowsx.h>

#include "context.h"
#include "window.h"
#include "application.h"


namespace nJinn {
	input::inputState input::sCurrentState;
	input::inputState input::sPreviousState;

	void input::update()
	{
		sPreviousState = sCurrentState;
		sCurrentState.mouseDeltaX = 0;
		sCurrentState.mouseDeltaY = 0;

		for (int i = 0; i < 256; ++i) {
			short state = GetAsyncKeyState(i);
			uint8_t val = 0;
			if (state & (1 << 15)) {
				val |= pressed;
				if (sPreviousState.keyboardState[i] & released) val |= justPressed;
			}
			else {
				val |= released;
				if (sPreviousState.keyboardState[i] & pressed) val |= justReleased;
			}
			sCurrentState.keyboardState[i] = val;
		}
	}

	void input::handleMessages(MSG & message)
	{
		if (message.message == WM_MOUSEMOVE) {
			sCurrentState.mousePosX = (int32_t) (GET_X_LPARAM(message.lParam));
			sCurrentState.mousePosY = (int32_t) (GET_Y_LPARAM(message.lParam));
			sCurrentState.mouseDeltaX = sCurrentState.mousePosX - sPreviousState.mousePosX;
			sCurrentState.mouseDeltaY = sCurrentState.mousePosY - sPreviousState.mousePosY;
		}
	}

	bool input::is(keyCode code, keyState state)
	{
		return 0 != (sCurrentState.keyboardState[code] & state);
	}

	void input::mousePosition(Eigen::Vector2d & position, bool pixels)
	{
		position.x() = sCurrentState.mousePosX;
		position.y() = sCurrentState.mousePosY;
		if (!pixels) {
			position = position.cwiseQuotient(ctx->mainWindow()->dimentions());
		}
	}
}