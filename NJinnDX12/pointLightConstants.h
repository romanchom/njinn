#ifndef POINTLIGHTCONSTANTS_H
#define POINTLIGHTCONSTANTS_H

#include "hlslTypeDefs.h"

struct pointLightConstants {
	float4x4 modelViewProjection;
	float4 color;
	float4 center;
};

#ifndef _WIN32
ConstantBuffer<pointLightConstants> light : register(b1);
#endif

#endif