#pragma once
#include "component.h"

#include <Eigen/Dense>

namespace nJinn {
	class camera : public component
	{
	private:
		Eigen::Matrix4d mView;
		Eigen::Matrix4d mProjection;
		Eigen::Matrix4d mViewProjection;
		double mNear;
		double mFar;
		double mFieldOfView;
	public:
		camera(gameObject * owner);
		virtual camera * copyTo(class gameObject * target) const override;
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

		double nearPlane() const { return mNear; }
		void nearPlane(double value) { mNear = value; }

		double farPlane() const { return mFar; }
		void farPlane(double value) { mFar = value; }

		double fieldOfView() const { return mFieldOfView; }
		void fieldOfView(double value) { mFieldOfView = value; }

		const Eigen::Matrix4d & viewMatrix() { return mView; }
		const Eigen::Matrix4d & projectionMatrix() { return mProjection; }
		const Eigen::Matrix4d & viewProjectionMatrix() { return mViewProjection; }

	protected:
		virtual void initialize() override;
		virtual void update() override;

	private:
		static camera * sMainCamera;
	public:
		static camera * main() { return sMainCamera; }
		static void main(camera * value) { sMainCamera = value; }
	};
}
