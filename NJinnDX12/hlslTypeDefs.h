#ifdef _WIN32

#include <Eigen/Dense>
#define float2 Eigen::Vector2f
#define float3 Eigen::Vector3f
#define float4 Eigen::Vector4f
#define float4x4 Eigen::Matrix4f
#define float3x3 Eigen::Matrix3f

#endif