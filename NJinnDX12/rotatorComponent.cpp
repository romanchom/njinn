#include "stdafx.h"
#include "rotatorComponent.h"
#include "gameObject.h"
#include <random>

namespace nJinn {
	rotatorComponent::rotatorComponent(gameObject * owner) : component(owner, 0)
	{
		t.reset();
		static std::default_random_engine r;
		std::uniform_real_distribution<double> d(0, 6.28);
		offset = d(r);
	}

	void rotatorComponent::update()
	{
		Eigen::Quaterniond rot(Eigen::AngleAxisd(-t.seconds() * 0.324445f + offset, Eigen::Vector3d::UnitZ()));
		mOwner->rotation(rot);
	}

	rotatorComponent * rotatorComponent::copyTo(class gameObject * target) const
	{
		rotatorComponent * ret = new rotatorComponent(target);
		ret->offset = offset;
		return ret;
	}
}
