#pragma once

namespace nJinn {
	struct geometryRootSig {
		static const int staticSamplerCount = 3;
		
		enum {
			perFrameCBLocation = 0,
			perObjectCBLocation,
			shadowCBLocation,
			perObjectTexturesLocation,
			perObjectSamplersLocation,
			index0,
			rootSigParamCount,
		};
	};
}