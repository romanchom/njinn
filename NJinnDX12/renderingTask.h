#pragma once

#include "task.h"
#include "bufferedCommandList.h"


namespace nJinn {
	class renderingTask : public task
	{
	protected:
		bufferedCommandList mCommandList;

		friend class deferredRenderer;
	};
}

