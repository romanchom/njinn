#include "stdafx.h"
#include "pointLightGS.h"

#include "dx12helper.h"
#include "context.h"
#include "geometryRootSig.h"

namespace nJinn{
	pointLightGS::pointLightGS(gameObject * owner) :
		pointLight(owner),
		mRenderTarget(mShadowMap),
		mShadowTask(this),
		mStopwatch(2)
	{
	}

	void pointLightGS::update()
	{
		pointLight::update();
		auto & profData = ctx->profiler.entries[L"Light GS"];
		profData.gpuTime = mStopwatch.getDifference(0, 1) * 1000;
		profData.cpuTime = mStopwatch.getCPUDifference(0, 1) * 1000;
	}

	void pointLightGS::generateShadowMap()
	{
		ctx->deferredRenderer.submitRenderingTask(&mShadowTask);
	}

	pointLightGS::shadowTask::shadowTask(pointLightGS * light) : mLight(light) {
		SET_NAME(mCommandList, L"Point shadow command list");
	};

	void pointLightGS::shadowTask::execute()
	{
		mLight->doGenerateShadowMap(mCommandList);
		mCommandList.close();
	}

	void pointLightGS::doGenerateShadowMap(ID3D12GraphicsCommandList * list)
	{
		mStopwatch.reset(list);
		mStopwatch.measure(0, list);

		list->SetGraphicsRootSignature(ctx->deferredRenderer.mRootSignature.Get());
		list->SetGraphicsRootConstantBufferView(geometryRootSig::shadowCBLocation, mShadowConstantsAddr);
		mRenderTarget.set(list);
		mRenderTarget.clear(list);
		drawShadowCastersGS(list);
		mRenderTarget.endFrame(list);

		mStopwatch.measure(1, list);
	}
}