#ifndef MESHDATA_H
#define MESHDATA_H

#include <cstdint>
#include <string>

#include "attributeTypes.h"

class meshData{
public:
	struct VertexAttribute{
		uint32_t type;
		uint8_t attributeIndex;
		uint8_t componentCount;
		uint8_t normalized;
		uint8_t offset;
		uint32_t typeSize();
		uint32_t size();
	};
	struct SBMHeader{
		uint32_t triangleCount;
		uint32_t vertexCount;
		uint16_t indexTypeSize;
		uint16_t vertexAttributeCount;
		SBMHeader() : triangleCount(0), indexTypeSize(0), vertexCount(0), vertexAttributeCount(0) {}
	};

	meshData() : vertexAttributes(nullptr), data(nullptr) {}
	~meshData();

	SBMHeader properties;
	VertexAttribute * vertexAttributes;
	uint8_t * data;

	uint32_t vertexSize() const;

	uint8_t * vertexData() const;
	uint32_t vertexDataSize() const;

	void * indexData() const;
	uint32_t indexDataSize() const;
	uint32_t indiciesCount() const;

	void allocate();
	void save(const std::wstring & filename, bool verbose = false);
	void load(const std::wstring & filename, bool verbose = false);
private:
	meshData(const meshData &) = delete;  // nie ma takiego kopiowania
	meshData & operator=(const meshData &) = delete;  // nie ma takiego kopiowania
};

#endif // MESHDATA_H