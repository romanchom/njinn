#ifndef TYPECONVERTER_H
#define TYPECONVERTER_H

#include <vector>
#include <limits>
#include <half.hpp>

#include "attributeTypes.h"
#include <cassert>


class TypeConverter{
public: 
	virtual void operator()(std::vector<uint8_t> & vector, const float * data, uint32_t count) const = 0;
	virtual uint32_t operator()(uint8_t * dst, const float * data, uint32_t count) const = 0;
	virtual uint32_t type() const = 0;
	virtual bool normalized() const = 0;
};

template<typename Dst>
class typeConverterD : public TypeConverter{
	virtual uint32_t type() const { return 0; }
};

template <typename Dst>
class RegularConverter : public typeConverterD<Dst>{
	virtual void operator()(std::vector<uint8_t> & vector, const float * data, uint32_t count) const;
	virtual uint32_t operator()(uint8_t * dst, const float * data, uint32_t count) const;
	virtual bool normalized() const { return false; }
};

template <typename Dst>
void RegularConverter<Dst>::operator()(std::vector<uint8_t> & vector, const float * data, uint32_t count) const {
	assert(count <= 4);
	Dst casted[4];
	for(uint32_t i = 0; i < count; ++i){
		casted[i] = (Dst) data[i];
	}
	vector.insert(vector.end(), (uint8_t *) casted, (uint8_t *)(casted + count));
}

template <typename Dst>
uint32_t RegularConverter<Dst>::operator()(uint8_t * dst, const float * data, uint32_t count) const {
	Dst * casted = (Dst *) dst;
	for(uint32_t i = 0; i < count; ++i){
		casted[i] = (Dst) data[i];
	}
	return sizeof(Dst) * count;
}

template <typename Dst>
class NormalizingConverter : public typeConverterD<Dst>{
	virtual void operator()(std::vector<uint8_t> & vector, const float * data, uint32_t count) const;
	virtual uint32_t operator()(uint8_t * dst, const float * data, uint32_t count) const;
	virtual bool normalized() const { return true; }
};

template <typename Dst>
void NormalizingConverter<Dst>::operator()(std::vector<uint8_t> & vector, const float * data, uint32_t count) const {
	assert(count <= 4);
	Dst casted[4];
	for(uint32_t i = 0; i < count; ++i){
		casted[i] = (Dst) (data[i] * (float) std::numeric_limits<Dst>::max());
	}
	vector.insert(vector.end(), (uint8_t *) casted, (uint8_t *) (casted + count));
}

template <typename Dst>
uint32_t NormalizingConverter<Dst>::operator()(uint8_t * dst, const float * data, uint32_t count) const {
	Dst * casted = (Dst *) dst;
	for(uint32_t i = 0; i < count; ++i){
		casted[i] = (Dst) (data[i] * (float) std::numeric_limits<Dst>::max());
	}
	return sizeof(Dst) * count;
}


template<>
uint32_t typeConverterD<float>::type() const { return FLOAT_T; }

template<>
uint32_t typeConverterD<uint8_t>::type() const { return UNSIGNED_BYTE_T; }

template<>
uint32_t typeConverterD<uint16_t>::type() const { return UNSIGNED_SHORT_T; }

template<>
uint32_t typeConverterD<uint32_t>::type() const { return UNSIGNED_INT_T; }

template<>
uint32_t typeConverterD<int8_t>::type() const { return BYTE_T; }

template<>
uint32_t typeConverterD<int16_t>::type() const { return SHORT_T; }

template<>
uint32_t typeConverterD<int32_t>::type() const { return INT_T; }

template<>
uint32_t typeConverterD<half_float::half>::type() const { return HALF_FLOAT_T; }

TypeConverter * getConverter(const std::string & type);

#endif // TYPECONVERTER_H