#include "MeshData.h"

#include <fstream>

uint32_t meshData::VertexAttribute::typeSize(){
	uint32_t ret(0);
	switch(type){
	case BYTE_T:							
	case UNSIGNED_BYTE_T:		ret = 1; break;
	case SHORT_T:							
	case UNSIGNED_SHORT_T:					
	case HALF_FLOAT_T:			ret = 2; break;
	case FLOAT_T:							
	case INT_T:							
	case UNSIGNED_INT_T:					
	case FIXED_T:				ret = 4; break;
	case DOUBLE_T:				ret = 8; break;
	}
	return ret;
}

uint32_t meshData::VertexAttribute::size(){
	return componentCount * typeSize();
}

uint32_t meshData::vertexSize() const{
	uint32_t ret(0);
	for(uint32_t i = 0; i < properties.vertexAttributeCount; ++i){
		ret += vertexAttributes[i].size();
	}
	return ret;
}

meshData::~meshData(){
	if(data) delete[] data;
	if(vertexAttributes) delete[] vertexAttributes;
}

uint8_t * meshData::vertexData() const{
	return data;
}

uint32_t meshData::vertexDataSize() const{
	return properties.vertexCount * vertexSize();
}

uint32_t meshData::indiciesCount() const {
	return properties.triangleCount * 3;
}

void * meshData::indexData() const{
	uint32_t offset = vertexDataSize();
	return (void *) (data + offset);
}

uint32_t meshData::indexDataSize() const{
	return properties.triangleCount * 3 * properties.indexTypeSize;
}

void meshData::allocate(){
	uint32_t bytes = vertexDataSize() + indexDataSize();
	if(data) delete data;
	data = new uint8_t[bytes];
}

void meshData::save(const std::wstring & filename, bool verbose){
	std::fstream file(filename.c_str(), std::ios::out | std::ios::binary);
	if(!file.good()){
		throw std::exception("Error saving to file\n");
	}

	if(verbose) printf("Writing binary data to file %s\n", filename.c_str());

	const uint32_t headerBytesCount = sizeof(properties);
	const uint32_t attributeBytesCount = sizeof(meshData::VertexAttribute) * properties.vertexAttributeCount;
	const uint32_t verticiesBytesCount = vertexDataSize();
	const uint32_t indiciesBytesCount = indexDataSize();

	std::runtime_error ex("Error writing to file");

	file.write((const char*) &properties, headerBytesCount);
	if (!file.good()) throw ex;
	file.write((const char*)vertexAttributes, attributeBytesCount);
	if (!file.good()) throw ex;
	file.write((const char*)vertexData(), verticiesBytesCount);
	if (!file.good()) throw ex;
	file.write((const char*)indexData(), indiciesBytesCount);
	if (!file.good()) throw ex;
	uint32_t bytes = headerBytesCount + attributeBytesCount + verticiesBytesCount + indiciesBytesCount;
	
	if(verbose) printf("Wrote %u bytes to file.\nExport succesfull!\n", bytes);
}

void meshData::load(const std::wstring & fileName, bool verbose){
	if (data) {
		throw std::exception("Cannot load mesh into already used object.");
	}

	std::fstream file(fileName, std::ios::in | std::ios::binary);

	if (!file.good()) {
		throw std::exception("Couldn't open file");
	}

	if (verbose) printf("Opened file %ls.\n", fileName.c_str());

	const uint32_t headerBytes = sizeof(properties);
	file.read((char *)&properties, headerBytes);
	if (file.eof()) throw std::exception("Too few header bytes read");

	vertexAttributes = new meshData::VertexAttribute[properties.vertexAttributeCount];
	const uint32_t vertexAttributeBytes = sizeof(meshData::VertexAttribute) * properties.vertexAttributeCount;
	file.read((char *)vertexAttributes, vertexAttributeBytes);
	if (file.eof()) throw std::exception("Too few header bytes read");

	uint32_t dataSize = vertexDataSize() + indexDataSize();
	data = new uint8_t[dataSize];

	file.read((char *)data, dataSize);
	if (file.eof()) throw std::exception("Too few verticies bytes read");
}
