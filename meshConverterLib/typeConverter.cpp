#include "typeConverter.h"


TypeConverter * getConverter(const std::string & type){
	TypeConverter * t;
	if(type == "float"){
		t = new RegularConverter<float>();
	} else if(type == "half"){
		t = new RegularConverter<half_float::half>();
	} else if(type == "double"){
		t = new RegularConverter<double>();
	} else if(type == "byte"){
		t = new RegularConverter<int8_t>();
	} else if(type == "byten"){
		t = new NormalizingConverter<int8_t>();
	} else if(type == "ubyte"){
		t = new RegularConverter<uint8_t>();
	} else if(type == "ubyten"){
		t = new NormalizingConverter<uint8_t>();
	} else if(type == "short"){
		t = new RegularConverter<int16_t>();
	} else if(type == "shortn"){
		t = new NormalizingConverter<int16_t>();
	} else if(type == "ushort"){
		t = new RegularConverter<uint16_t>();
	} else if(type == "ushortn"){
		t = new NormalizingConverter<uint16_t>();
	} else if(type == "int"){
		t = new RegularConverter<int32_t>();
	} else if(type == "intn"){
		t = new NormalizingConverter<int32_t>();
	} else if(type == "uint"){
		t = new RegularConverter<uint32_t>();
	} else if(type == "uintn"){
		t = new NormalizingConverter<uint32_t>();
	} else throw std::exception("Unrecognized data type");
	return t;
}
